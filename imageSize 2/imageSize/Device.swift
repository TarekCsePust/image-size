//
//  Device.swift
//  imageSize
//
//  Created by Odyssey_New on 10/20/19.
//  Copyright © 2019 Odyssey_New. All rights reserved.
//

import Foundation
import UIKit

struct Device {
  
    static let getInit = Device()
    // Device.init().isIphone
    var isIphone: Bool{
        return UIDevice.current.userInterfaceIdiom == .phone
    }
    
    var isPod: Bool{
        return UIDevice.current.userInterfaceIdiom == .pad
    }
    
    var isIphoneX: Bool {
        if #available(iOS 11.0,  *) {
            return UIApplication.shared.delegate?.window??.safeAreaInsets.top ?? 0 > 20
        }
        
        return false
    }
    
}
