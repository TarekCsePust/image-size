//
//  canVasViewController.swift
//  imageSize
//
//  Created by Odyssey_New on 11/4/19.
//  Copyright © 2019 Odyssey_New. All rights reserved.
//

import UIKit
import AVKit
import Photos
class canVasViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var canvasView: UIView!
    var image:UIImage!
    var orginalImage:UIImage!
    var ratioView: UIImageView!
    var ratioViewBg: UIImageView!
    var topSafeAreaHeight: CGFloat = 0
    var bottomSafeAreaHeight: CGFloat = 0
    var isEditImg = true
    var isEdit = false
    var canvasViewClick: CanvasViewControllerDelegate?
    
    
    var collectionView_Ratio: UICollectionView!
    var aspectRatio: [String] =
        [
            "1:1",
            "2:3",
            "3:2",
            "3:4",
            "4:3",
            "4:3",
            "2:3",
            "2.6:1",
            "2:1",
            "2.7:1",
            "1.9:1",
            "4:5",
            "1.91:1",
            "16:9",
            "3:1",
            "2:3",
            "1.4:1",
            "2:1",
            "16:9",
            "1.8:1",
            "16:25",
            "4:1",
            "2:1"
    ]
    
    var cellSize: [CGSize] = [
        CGSize(width: 65, height: 105),
        CGSize(width: 65, height: 105),
        CGSize(width: 43, height: 105),
        CGSize(width: 65, height: 105),
        CGSize(width: 49, height: 105),
        CGSize(width: 65, height: 105),
        CGSize(width: 65, height: 105),
        CGSize(width: 43, height: 105),
        CGSize(width: 65, height: 105),
        CGSize(width: 65, height: 105),
        CGSize(width: 65, height: 105),
        CGSize(width: 65, height: 105),
        CGSize(width: 52, height: 105),
        CGSize(width: 65, height: 105),
        CGSize(width: 65, height: 105),
        CGSize(width: 65, height: 105),
        CGSize(width: 43, height: 105),
        CGSize(width: 65, height: 105),
        CGSize(width: 65, height: 105),
        CGSize(width: 65, height: 105),
        CGSize(width: 65, height: 105),
        CGSize(width: 42, height: 105),
        CGSize(width: 65, height: 105),
        CGSize(width: 65, height: 105)
    ]
    
    var titleList_Ratio : [String] =
        [
          //  NSLocalizedString("Original", comment: "title"),
            NSLocalizedString("Squere", comment: "title"),
            NSLocalizedString("2:3", comment: "title"),
            NSLocalizedString("3:2", comment: "title"),
            NSLocalizedString("3:4", comment: "title"),
            NSLocalizedString("4:3", comment: "title"),
            NSLocalizedString("Facebook Landscape Post", comment: "title"),
            NSLocalizedString("Facebook Portrait Post", comment: "title"),
            NSLocalizedString("Facebook Page Cover Desktop", comment: "title"),
            NSLocalizedString("Facebook Page Cover Mobile", comment: "title"),
            NSLocalizedString("Facebook Profile Cover", comment: "title"),
            NSLocalizedString("Facebook Event Cover", comment: "title"),
            NSLocalizedString("Instagram Portrait Post", comment: "title"),
            NSLocalizedString("Instagram Landscape Post", comment: "title"),
            NSLocalizedString("Twitter Post Landscape", comment: "title"),
            NSLocalizedString("Twitter Header", comment: "title"),
            NSLocalizedString("Pinterest Pin", comment: "title"),
            NSLocalizedString("LinkedIn Post", comment: "title"),
            NSLocalizedString("LinkedIn Background", comment: "title"),
            NSLocalizedString("Youtube Video Thumbnail", comment: "title"),
            NSLocalizedString("Youtube Channel Cover", comment: "title"),
            NSLocalizedString("Wattpad Cover", comment: "title"),
            NSLocalizedString("Etsy Cover", comment: "title"),
            NSLocalizedString("Eventbrite Cover Image", comment: "title")
    ]
    
    var image_Ratio =
        [
            UIImage(named: "1-Square-1-1.png"),
            UIImage(named: "2-2-3.png"),
            UIImage(named: "3-3-2.png"),
            UIImage(named: "4-3-4.png"),
            UIImage(named: "5-4-3.png"),
            UIImage(named: "6-Facebook-Landscape-Post-4-3.png"),
            UIImage(named: "7-Facebook-Portrait-Post-2-3.png"),
            UIImage(named: "8-Facebook-Page-Cover-Desktop-2.6-1.png"),
            UIImage(named: "9-Facebook-Page-Cover-Mobile-2-1.png"),
            UIImage(named: "10-Facebook-Profile-Cover-2.7-1.png"),
            UIImage(named: "11-Facebook-Event-Cover-1.9-1.png"),
            UIImage(named: "12-Instagram-Portrait-Post-4-5.png"),
            UIImage(named: "13-Instagram-Landscape-Post-1.9-1.png"),
            UIImage(named: "14-Twitter-Post-Landscape-16-9.png"),
            UIImage(named: "15-Twitter-Header-3-1.png"),
            UIImage(named: "16-Pinterest-Pin-2-3.png"),
            UIImage(named: "17-LinkedIn-Post-1.4-1.png"),
            UIImage(named: "18-LinkedIn-Background-2-1.png"),
            UIImage(named: "19-Youtube-Video-Thumbnail-16-9.png"),
            UIImage(named: "20-Youtube-Channel-Cover-1.8-1.png"),
            UIImage(named: "21-Wattpad-Cover-2-3.png"),
            UIImage(named: "22-Etsy-Cover-4-1.png"),
            UIImage(named: "23-Eventbrite-Cover-Image-2-1.png")
    ]
    var statusBarView = UIView()
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
      //  var t = ColorList().count
        self.navigationController?.isNavigationBarHidden = true
        self.setUpView()

        getSafeAria()
        let h = self.view.frame.height - (topSafeAreaHeight + bottomSafeAreaHeight + 50 + 125)
        canvasView.frame = CGRect(x:0,y:topSafeAreaHeight+50,width: self.view.frame.width,height: h)
        ratioView = UIImageView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: h))
        ratioViewBg = UIImageView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: h))
        ratioView.image = image
        ratioView.contentMode = .scaleAspectFit;
        ratioView.backgroundColor = .clear
        
        canvasView.addSubview(ratioViewBg)
        ratioViewBg.addSubview(ratioView)
    //    print("frame: \(ratioView.frame)")
      //  print("frame: \(canvasView.frame)")
        //print("frame: \(ratioViewBg.frame)")
        // Do any additional setup after loading the view.
    }
    
    
    
    func setUpView() {
           if #available(iOS 11.0, *) {
               statusBarView.frame = CGRect(x: 0, y: (UIApplication.shared.keyWindow?.safeAreaInsets.top)!, width: self.view.frame.width, height: 50)
               if UIDevice.current.userInterfaceIdiom == .phone {
                   if UIScreen.main.nativeBounds.height == 2436 {
                       statusBarView.frame = CGRect(x: 0, y: (UIApplication.shared.keyWindow?.safeAreaInsets.top)! - 30, width: self.view.frame.width, height: 40)
                   }
               }
           } else {
               statusBarView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 30)
           }
           statusBarView.backgroundColor = UIColor(red: 18/255, green: 18/255, blue: 18/255, alpha: 1)
           self.view.addSubview(statusBarView)
           
           ///cancel button
           let cancelButton = UIButton()
           cancelButton.frame = CGRect(x: 5, y: statusBarView.frame.height/2 - 10, width: (self.view.frame.width - 10)/3, height: 30)
           cancelButton.setTitle(NSLocalizedString("Cancel", comment: "title"), for: .normal)
           cancelButton.setTitleColor(UIColor.white, for: .normal)
           cancelButton.titleLabel?.font = UIFont(name: "Helvetica", size: 15)
           cancelButton.contentHorizontalAlignment = .left
            cancelButton.addTarget(self, action: #selector(cancelButtonAction), for: .touchDown)
           statusBarView.addSubview(cancelButton)
           
           ///crop label
           let cropLabel = UILabel()
           cropLabel.frame = CGRect(x: cancelButton.frame.maxX, y: statusBarView.frame.height/2 - 10, width: (self.view.frame.width - 10)/3, height: 30)
           cropLabel.text = (NSLocalizedString("Canvas", comment: "title"))
           cropLabel.textColor = UIColor.white
           cropLabel.font = UIFont(name: "Helvetica-Bold", size: 18)
           cropLabel.textAlignment = .center
           statusBarView.addSubview(cropLabel)
           
           ///done button
           let doneButton = UIButton()
           doneButton.frame = CGRect(x: cropLabel.frame.maxX, y: statusBarView.frame.height/2 - 10, width: (self.view.frame.width - 10)/3, height: 30)
           doneButton.setTitle(NSLocalizedString("Done", comment: "title"), for: .normal)
           doneButton.setTitleColor(UIColor.white, for: .normal)
           doneButton.titleLabel?.font = UIFont(name: "Helvetica", size: 15)
           doneButton.contentHorizontalAlignment = .right
           doneButton.addTarget(self, action: #selector(doneButtonAction), for: .touchDown)
           statusBarView.addSubview(doneButton)
           
         
           
           ///ratio collection view
           let flowLayout = UICollectionViewFlowLayout()
           flowLayout.scrollDirection = .horizontal
           
           if #available(iOS 11.0, *) {
               collectionView_Ratio = UICollectionView(frame: CGRect(x: 0, y: self.view.frame.height - 110 - (UIApplication.shared.keyWindow?.safeAreaInsets.bottom)!, width: self.view.frame.width, height: 125), collectionViewLayout: flowLayout)
           } else {
               collectionView_Ratio = UICollectionView(frame: CGRect(x: 0, y: self.view.frame.height - 110, width: self.view.frame.width, height: 125), collectionViewLayout: flowLayout)
           }
           collectionView_Ratio.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "Cell_Ratio")
           collectionView_Ratio.delegate = self
           collectionView_Ratio.dataSource = self
           collectionView_Ratio.showsHorizontalScrollIndicator = false
           collectionView_Ratio.backgroundColor = UIColor.black.withAlphaComponent(0.7)
           self.view.addSubview(collectionView_Ratio)
           
       
       }
    
       func resetUI()
       {
         let h = self.view.frame.height - (topSafeAreaHeight + bottomSafeAreaHeight + 50 + 125)
          canvasView.frame = CGRect(x:0,y:topSafeAreaHeight+50,width: self.view.frame.width,height: h)
          ratioView = UIImageView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: h))
          ratioViewBg = UIImageView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: h))
       }
       
       @objc func cancelButtonAction() {
           
        
           print("press canvas cancel buttton")
          navigationController?.popViewController(animated: true)

          dismiss(animated: true, completion: nil)
         
       }
       
       @objc func doneButtonAction() {
           print("press canvas done button")
           
          if isEdit{
           
             // canvasViewClick?.stViewItemClick(img: createCanvasImg())
              //navigationController?.popViewController(animated: true)

             //  dismiss(animated: true, completion: nil)
            
            
        //    print("ratiobg in done: \(ratioViewBg.frame)")
          //  print("ratioView in done: \(ratioView.frame)")
           
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "viewController") as? ViewController
            vc?.ImageFrameRatioBG = ratioViewBg.frame
            vc?.ImageViewRatio = ratioView.frame
            
            vc?.image = self.image
            vc?.mainImage = self.orginalImage
          //  vc?.image = createCanvasImg()
            self.navigationController?.pushViewController(vc!, animated: true)
          }
          
           
       }
    
      func createCanvasImg()-> UIImage{
        
        UIGraphicsBeginImageContextWithOptions(ratioViewBg.frame.size, false, UIScreen.main.scale)
         
         ratioViewBg.layer.render(in: UIGraphicsGetCurrentContext()!)
         let rowImage = UIGraphicsGetImageFromCurrentImageContext()
         UIGraphicsEndImageContext()
         return rowImage!
        
    }
    
      
     func numberOfSections(in collectionView: UICollectionView) -> Int {
            return 1
        }
        
        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return titleList_Ratio.count
        }
        
        func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell_Ratio", for: indexPath)
            
            for view in cell.subviews {
                view.removeFromSuperview()
            }
            
            ///imageView
            let imageView = UIImageView()
            imageView.frame = CGRect(x: 0, y: 0, width: cell.frame.width, height: cell.frame.height - 40)
            
             imageView.image = image_Ratio[indexPath.row]
          /*  if indexPath.row == 0 {
                imageView.image = self.image
            }
            else {
                imageView.image = image_Ratio[indexPath.row-1]
            }*/
            cell.addSubview(imageView)
            
            ///imageView lock
           /* if indexPath.row > 0 && !"f5videomixerpro".isPurchased() {
                let imageViewLock = UIImageView()
                imageViewLock.frame = CGRect(x: cell.frame.width - 10, y: cell.frame.height - 40 - 40, width: 10, height: 10)
                imageViewLock.image = UIImage(named: "roundLock.png")
                cell.addSubview(imageViewLock)
            }
           */
            
            ///label
            let label = UILabel()
            label.frame = CGRect(x: 0, y: imageView.frame.maxY, width: cell.frame.width, height: 40)
            label.text = titleList_Ratio[indexPath.row]
            label.textColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1.0)
            label.textAlignment = .center
            label.font = UIFont.systemFont(ofSize: 9)
            label.numberOfLines = 0
            cell.addSubview(label)
            
            return cell
        }
        
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            
            return cellSize[indexPath.row] //as! CGSize
        }
        
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
            return 10.0
        }
        
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
            return 10.0
        }
        
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
            return UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
        }
        
        func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            if indexPath.row < 0 {
               // ratioViewBg.image = nil
                //resetUI()
                print("click first crop item")
               
            }
            else {
                
                ratioViewBg.backgroundColor = UIColor.white
            //   if(isEditImg){ratioViewBg.image = image}
            //   print("ratio: \(aspectRatio[indexPath.row])")
               let frameSize = getSizeFromRatio(ratio: aspectRatio[indexPath.row])
               
               
               
             
               if(frameSize.height > frameSize.width){
                   let x = (canvasView.frame.width - frameSize.width)/2
                   ratioView.frame = CGRect(x: 0, y: 0, width: frameSize.width, height: frameSize.height)
                   ratioViewBg.frame = CGRect(x: x, y: 0, width: frameSize.width, height: frameSize.height)
                   
                 
                   
                   
               }else{
                   let y = (canvasView.frame.height - frameSize.height)/2
                   ratioView.frame = CGRect(x: 0, y: 0, width: frameSize.width, height: frameSize.height)
                   ratioViewBg.frame = CGRect(x: 0, y: y, width: frameSize.width, height: frameSize.height)
                   
                
               }
                isEdit = true
               // ratioView.isHidden = true
                print("ratiobg: \(ratioViewBg.frame)")
                print("ratioView: \(ratioView.frame)")
               
             
            }
        }
    
    
    func getSizeFromRatio(ratio:String )-> CGSize{
        
           let elements = ratio.components(separatedBy: ":")
           let aspectRatioWidth = CGFloat(Float(elements.first!)!)
           let aspectRatioHeight = CGFloat(Float(elements.last!)!)
         //  print("\n\nratio : \(aspectRatioWidth) : \(aspectRatioHeight)\n\n")
           let canvasW = canvasView.frame.width
           let canvasH = canvasView.frame.height - bottomSafeAreaHeight
           
           if(aspectRatioHeight > aspectRatioWidth){
               let r = canvasH / aspectRatioHeight
               let w = aspectRatioWidth * r
               let h = aspectRatioHeight * r
               
               return CGSize(width: w, height: h)
           }else{
               
               let r = canvasW / aspectRatioWidth
               let w = aspectRatioWidth * r
               let h = aspectRatioHeight * r
               
               return CGSize(width: w, height: h)
               
           }
           
           
       }

    func getSafeAria(){
       
       if #available(iOS 11.0, *) {
           let window = UIApplication.shared.windows[0]
           let safeFrame = window.safeAreaLayoutGuide.layoutFrame
           topSafeAreaHeight = safeFrame.minY
           bottomSafeAreaHeight = window.frame.maxY - safeFrame.maxY
       }
        
    }
       

}


protocol CanvasViewControllerDelegate
{
    func stViewItemClick(img :UIImage)
}
