//
//  ViewController.swift
//  imageSize
//
//  Created by Odyssey_New on 10/17/19.
//  Copyright © 2019 Odyssey_New. All rights reserved.
//

import UIKit
import IGRPhotoTweaks

struct Item{
    var itemName : String
}

class ViewController: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate, UICollectionViewDelegateFlowLayout  {
    
  
    
    
    
    var selectedFilter: Int!
    var slideShowCount = 0
    var clickCount = 0
    var prevItem:Int?
    var nextItem:Int?
    var filteredImage:UIImage!
    var context = CIContext(options: nil)
    var aspectSize:CGSize!
    
    var items:[Item] = [
           Item(itemName:"new"),Item(itemName:"crop"), Item(itemName:"size"), Item(itemName:"filter"),
           Item(itemName:"presets"),Item(itemName:"canvas"),Item(itemName:"color"),
           ]
    var filterCellImage:UIImage!
    var filterItems:[Item] = [
            Item(itemName:"HDR"), Item(itemName:"Dodger"), Item(itemName:"Noise"),  Item(itemName:"Film"), Item(itemName:"Glitch"),Item(itemName:"GRNG"), Item(itemName:"VHS"), Item(itemName:"Wave"),  Item(itemName:"VIN 1"), Item(itemName:"VIN 2"),Item(itemName:"VIN 3"), Item(itemName:"VIN 4"), Item(itemName:"MIL"),  Item(itemName:"MIL 2"), Item(itemName:"MIL 3"),Item(itemName:"MIL 4"), Item(itemName:"Seafoam"), Item(itemName:"Drama"),  Item(itemName:"B/W"), Item(itemName:"B/W Hicon"),
            Item(itemName:"Film B/W"), Item(itemName:"MV3"),  Item(itemName:"B/W Lowcon"), Item(itemName:"MV4"),
            Item(itemName:"Cinerama"), Item(itemName:"Ivory"),  Item(itemName:"Warm Color"), Item(itemName:"Light Cross"),
            Item(itemName:"Sun 1"), Item(itemName:"Sun 2"),Item(itemName:"Sun 3"), Item(itemName:"Crisp"),
            Item(itemName:"Vibrant"), Item(itemName:"MNCH1"),  Item(itemName:"MNCH2"), Item(itemName:"MNCH3"),
            Item(itemName:"BLH1"), Item(itemName:"BLH2"),  Item(itemName:"BLH3"),
            Item(itemName:"Retro"), Item(itemName:"Sunny"),  Item(itemName:"KPP1"), Item(itemName:"KPP2"),
            Item(itemName:"KPP3"), Item(itemName:"Vintage"),  Item(itemName:"Warm Amber"),
            Item(itemName:"Sharpen"), Item(itemName:"HDR2"),  Item(itemName:"B/W HDR"),Item(itemName:"MV1"),
              Item(itemName:"MV2"), Item(itemName:"MV5"),Item(itemName:"Vignette"), Item(itemName:"Lomo"), Item(itemName:"Twillight"), Item(itemName:"B/W Cross"),Item(itemName:"B/W Vintage"), Item(itemName:"Sepia"),Item(itemName:"Cross Proc 1"), Item(itemName:"Cross Proc 2")
            ]
    
    let cellIdentifier = "itemCollectionViewCell"
    var collectionViewFlowLayout: UICollectionViewFlowLayout!
    
    let cellIdentifier2 = "filterCollectionViewCell"
     var collectionViewFlowLayout2: UICollectionViewFlowLayout!
    
    //@IBOutlet weak var imgView: UIImageView!
    var image:UIImage?
    var mainImage:UIImage!
    var sizeImageDetect:Int!
    var resetImageDetect:Int!
    var ImageFrameRatioBG:CGRect!
    var ImageViewRatio:CGRect!
    var colorImage:UIImage!
    var colorIndex:Int!
    var hasColorBG:Int!
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var filterCollectionView: UICollectionView!
    
    //@IBOutlet weak var pickImageBtn: UIButton!
    @IBOutlet weak var imgView: UIImageView!
    
    @IBOutlet weak var imageView2: UIImageView!
    
  //  var imgView2 : UIImageView?
    
    @IBOutlet weak var slideView: UIView!
    
    @IBOutlet weak var slider1: UISlider!
    
    
    @IBOutlet weak var slider2: UISlider!
    
    @IBOutlet weak var slide1Label: UILabel!
    
    @IBOutlet weak var slider3: UISlider!
    
    
    @IBOutlet weak var slider4: UISlider!
    
    @IBOutlet weak var slide2Label: UILabel!
    
    @IBOutlet weak var slide3Label: UILabel!
    
    @IBOutlet weak var slide4Label: UILabel!
    
    
    @IBOutlet weak var cropButton: UIButton!
    
    
  //  @IBOutlet weak var saveBtn: UIButton!
    
    @IBOutlet weak var imageFrameUiView: UIView!
    var imagePicker = UIImagePickerController()
    
    var cropeImage: UIImage?

    let screenSize : CGRect = UIScreen.main.bounds
      
    let screenHeight : CGFloat = UIScreen.main.bounds.height
      
    let screenWidth : CGFloat = UIScreen.main.bounds.width
    
    var originalImageWidth:CGFloat!
    var originalImageHeight:CGFloat!
    
 //   @IBOutlet weak var size1Btn: UIButton!
    
    
   // @IBOutlet weak var size2Btn: UIButton!
    
    //@IBOutlet weak var size3Btn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imagePicker.delegate = self
        sizeImageDetect = 0
        resetImageDetect = 0
        slideShowCount  = 0
        clickCount = 0
        cropButton.isHidden = true
     //   self.navigationController?.isNavigationBarHidden = true
        print(screenWidth)
        print(screenHeight)
        filterCellImage = UIImage(named: "size")
     
     //   self.image = UIImage(named: "crop")
        //var navigationBarHeight: CGFloat = self.navigationController!.navigationBar.frame.height
        // temp space for navigation bar
       var temp = CGFloat(70)
        
        if ImageFrameRatioBG != nil && ImageViewRatio != nil
        {
           
                 print("call in canvas hidden view")
                self.imageFrameUiView.frame = ImageFrameRatioBG
                self.imageFrameUiView.frame.origin.y = temp
                self.imgView.frame = ImageViewRatio
                self.imgView.image = self.image
            
        }
        else
        {
            imageFrameUiView.frame.origin.y = temp
            imageFrameUiView.frame.origin.x = 0
            imageFrameUiView.frame.size.width = screenWidth
            imageFrameUiView.frame.size.height =  screenWidth
            imgView.frame.origin.x = 0
            imgView.frame.origin.y = 0
            imgView.frame.size.width = screenWidth
            imgView.frame.size.height = screenWidth
        }
        
        
       /* imageFrameUiView.frame.origin.y = temp
        imageFrameUiView.frame.origin.x = 0
        imageFrameUiView.frame.size.width = screenWidth
        imageFrameUiView.frame.size.height =  screenWidth
        imgView.frame.origin.x = 0
        imgView.frame.origin.y = 0
        imgView.frame.size.width = screenWidth
        imgView.frame.size.height = screenWidth
         */
        imageView2.frame.origin.x = 0
        imageView2.frame.origin.y = 0
        imageView2.frame.size.width = self.imageFrameUiView.frame.size.width
        imageView2.frame.size.height = self.imageFrameUiView.frame.size.height
         //imageView2.isHidden = true
      if hasColorBG != nil && hasColorBG == 1
        {
            imageView2.isHidden = false
            if colorIndex != nil
            {
                 imageView2.image = image
                 imageView2.image = imageView2.image?.withRenderingMode(.alwaysTemplate)
                 imageView2.tintColor = ColorList()[colorIndex]
            }
            else
            {
                imageView2.image = colorImage
            }
        }
        else
        {
             imageView2.isHidden = true
        }
         
        
        
        
        self.imageFrameUiView.addSubview(imgView)
   
        filterCollectionView.frame = CGRect(x:0,y:485,width: screenWidth,height: 80)
        filterCollectionView.isHidden = true
        collectionView.frame.size.width = screenWidth
       
        
        var t = imageFrameUiView.frame.size.height
        var h = filterCollectionView.frame.minY - temp
        var remH = (h - t)/2
        imageFrameUiView.frame.origin.y = imageFrameUiView.frame.origin.y + remH
        setupCollectionView()
        
        imageFrameUiView.clipsToBounds = true
        imgView.clipsToBounds = true
        
        let pangesture = UIPanGestureRecognizer(target: self, action: #selector(self.panGesture))
        imgView.isUserInteractionEnabled = true
        imgView.addGestureRecognizer(pangesture)
        
        
        let pinchGesture = UIPinchGestureRecognizer(target: self, action:#selector(pinchGesture(sender:)))
        
        imgView.addGestureRecognizer(pinchGesture)
        
        
       let rotationgesture = UIRotationGestureRecognizer(target: self, action: #selector(self.rotationGesture))
        imgView.addGestureRecognizer(rotationgesture)
        let doneButton = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(done))
        self.navigationItem.rightBarButtonItem = doneButton
        
        let backButton = UIBarButtonItem(title: "Back", style: .done, target: self, action: #selector(back))
               self.navigationItem.leftBarButtonItem = backButton
        let nav = self.navigationController?.navigationBar
         nav?.barStyle = UIBarStyle.black
         nav?.tintColor = UIColor.white
        
        
        
        
        
        var tem = screenWidth-30
        
        var tem2 = tem-12
        slide1Label.frame = CGRect(x: 10, y: 5, width: tem2, height: 21)
        slider1.frame = CGRect(x: 6, y: 29, width: tem2, height: 30)
        
        slide2Label.frame = CGRect(x: 10, y: 64, width: tem2, height: 21)
        slider2.frame = CGRect(x: 6, y: 88, width: tem2, height: 30)
        
        slide3Label.frame = CGRect(x: 10, y: 123, width: tem2, height: 21)
        slider3.frame = CGRect(x: 6, y: 147, width: tem2, height: 30)
        
        slide4Label.frame = CGRect(x: 10, y: 182, width: tem2, height: 21)
        slider4.frame = CGRect(x: 6, y: 206, width: tem2, height: 30)
        
        var tem3 = slider4.frame.minY+30+5
        var tem4 = collectionView.frame.minY-30-tem3
        slideView.frame = CGRect(x:15,y:tem4,width:tem,height: tem3)
        slideView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        slideView.isHidden = true
        slideView.layer.cornerRadius = 5.0
        slideView.clipsToBounds = true
        
        
        slider1.addTarget(self, action: #selector(slider1DidEndSliding), for: [.touchUpInside, .touchUpOutside])
        slider2.addTarget(self, action: #selector(slider2DidEndSliding), for: [.touchUpInside, .touchUpOutside])
        slider3.addTarget(self, action: #selector(slider3DidEndSliding), for: [.touchUpInside, .touchUpOutside])
        slider4.addTarget(self, action: #selector(slider4DidEndSliding), for: [.touchUpInside, .touchUpOutside])
       
        let thumbImage = UIImage(named: "slider-point.png")
        slider1.setThumbImage(thumbImage, for: .normal)
        slider2.setThumbImage(thumbImage, for: .normal)
        slider3.setThumbImage(thumbImage, for: .normal)
        slider4.setThumbImage(thumbImage, for: .normal)
        
        slide1Label.text = "Fade"
        slide2Label.text = "Vignette"
        slide3Label.text = "Gamma"
        slide4Label.text = "Intensity"
        
        
        print("call again and again")
     // self.navigationController?.isNavigationBarHidden = false
        // Do any additional setup after loading the view.
    }
    
    
    override func viewDidAppear(_ animated: Bool)
    {
         self.navigationController?.isNavigationBarHidden = false
        if ImageViewRatio != nil && ImageFrameRatioBG != nil
        {
           
          
            
           // self.imageFrameUiView.isHidden = false
        /*   self.imageFrameUiView.frame = ImageFrameRatioBG
           // self.imageFrameUiView.frame.origin.y = self.imageFrameUiView.frame.origin.y
            self.imgView.frame = ImageViewRatio
            self.imgView.image = self.image
           */
            print("image size in did appear: \(self.image?.size)")
            imageView2.frame.origin.x = 0
            imageView2.frame.origin.y = 0
            imageView2.frame.size.width = self.imageFrameUiView.frame.size.width
            imageView2.frame.size.height = self.imageFrameUiView.frame.size.height
           
            
            filterCellImageSize()
        }
        
        
    }
    
    
    
    
    @IBAction func clickCrop(_ sender: Any) {
        
        print("Crop")
       // self.performSegue(withIdentifier: "goToEditView", sender: imgView.image)
    }
    
    
    @objc func done()
    {
        
        if(slideView.isHidden == false)
        {
           slideView.isHidden = true
           slideShowCount = 1
        }
        else
        {
            
        }
    }
    
    @objc func back()
    {
        if(slideView.isHidden == false)
        {
           slideView.isHidden = true
           slideShowCount = 1
        }
        else
        {
            if self.collectionView.isHidden == true
            {
                self.filterCollectionView.isHidden = true
                self.collectionView.isHidden = false
            }
        }
        
    }
    
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
           
           let touch = touches.first!
           let location = touch.location(in: self.view)
         
           var x = location.x
           var y = location.y
         
           if((x < slideView.frame.minX || x > slideView.frame.maxX) || (y < slideView.frame.minY || y > slideView.frame.maxY))
           {
               if(slideView.isHidden == false)
               {
                   slideView.isHidden = true
                   slideShowCount = 1
               }
           }
        
       }
    
    
    @objc func slider1DidEndSliding()
    {
        slideView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        slider2.isHidden = false
        slider3.isHidden = false
        slider4.isHidden = false
        slide2Label.isHidden = false
        slide3Label.isHidden = false
        slide4Label.isHidden = false
    }
    
    @objc func slider2DidEndSliding()
    {
          slideView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
          slider1.isHidden = false
          slider3.isHidden = false
          slider4.isHidden = false
          slide1Label.isHidden = false
          slide3Label.isHidden = false
          slide4Label.isHidden = false
    }
    
    @objc func slider3DidEndSliding()
    {
          slideView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
          slider2.isHidden = false
          slider1.isHidden = false
          slider4.isHidden = false
          slide2Label.isHidden = false
          slide1Label.isHidden = false
          slide4Label.isHidden = false
    }
    
    @objc func slider4DidEndSliding()
    {
          slideView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
          slider2.isHidden = false
          slider3.isHidden = false
          slider1.isHidden = false
          slide2Label.isHidden = false
          slide3Label.isHidden = false
          slide1Label.isHidden = false
    }
    func setupSlider1Defaults()
    {
        slider1.maximumValue = 1
        slider1.minimumValue = 0
        slider1.value = 0.8
    }
    
    func setupSlider2Defaults()
    {
        slider2.maximumValue = getRedius(percent: 70)
        slider2.minimumValue = 0.1
    }
    
    func getRedius(percent:CGFloat)->Float
    {
        let t1 = ((self.image?.size.width)! + (self.image?.size.height)!)/2.0
        let t2 = (t1*percent)/100
        return Float(t2)
      
    }
    
    
    func setupSlider3Defaults()
    {
        slider3.maximumValue = 1
        slider3.minimumValue = 0
    }
    
    func setupSlider4Defaults()
    {
        slider4.maximumValue = 1
        slider4.minimumValue = 0
    }
    
    @IBAction func slider1Moved(_ sender: Any) {
        
          slideView.backgroundColor = UIColor.black.withAlphaComponent(0.0)
          slider2.isHidden = true
          slider3.isHidden = true
          slider4.isHidden = true
          
         
          slide2Label.isHidden = true
          slide3Label.isHidden = true
          slide4Label.isHidden = true
          
          effectFade()
    }
    
    
    @IBAction func slider2Moved(_ sender: Any) {
        
         slideView.backgroundColor = UIColor.black.withAlphaComponent(0.0)
         slider1.isHidden = true
         slider3.isHidden = true
         slider4.isHidden = true
         
        
         slide1Label.isHidden = true
         slide3Label.isHidden = true
         slide4Label.isHidden = true
         imgView.image = vignette()
    }
    
    
    @IBAction func slider3Moved(_ sender: Any) {
        
        slideView.backgroundColor = UIColor.black.withAlphaComponent(0.0)
        slider2.isHidden = true
        slider1.isHidden = true
        slider4.isHidden = true
         
        slide2Label.isHidden = true
        slide1Label.isHidden = true
        slide4Label.isHidden = true
        imgView.image = gamma()
    }
    
    @IBAction func slider4Moved(_ sender: Any) {
        
         slideView.backgroundColor = UIColor.black.withAlphaComponent(0.0)
         slider2.isHidden = true
         slider3.isHidden = true
         slider1.isHidden = true
         
        
         slide2Label.isHidden = true
         slide3Label.isHidden = true
         slide1Label.isHidden = true
         imgView.image = intensity()
    }
    
    func effectFade()
    {
          var im = filteredImage
          let img = im!.imageWithAlpha(alpha:CGFloat(1-slider1.value))
         
          imgView.image = img
    }
    
    func vignette() -> UIImage
     {
         var image:UIImage?
         var center:CIVector?
         
         image = filteredImage
         center = CIVector(x: (image!.size.width)/2, y: (image!.size.height)/2)
         
       //  let context = CIContext(options: nil)
         let ciImage = CIImage(image: image!)
         
     
         let Cropped = CIFilter(name: "CIVignetteEffect")
         Cropped?.setDefaults()
         Cropped?.setValue(ciImage, forKey: "inputImage")
         Cropped?.setValue(center!, forKey: "inputCenter")
         Cropped?.setValue((slider2.maximumValue-slider2.value), forKey: "inputRadius")
         Cropped?.setValue(0.3, forKey: "inputIntensity")
         let originalImage2CG = CIImage(cgImage: (image?.cgImage)!)
         if let output = Cropped?.outputImage
         {
             if let cgimg = context.createCGImage(output, from: originalImage2CG.extent)
             {
                 let processedImage = UIImage(cgImage: cgimg)
                 
                 return processedImage
             }
         }
         return image!
     }
     
     func intensity() -> UIImage
     {
       
         var image:UIImage?
         image = filteredImage
         let ciImage = CIImage(image: image!)
         
         let Cropped = CIFilter(name: "CIExposureAdjust")
         Cropped?.setDefaults()
         Cropped?.setValue(ciImage, forKey: "inputImage")
         Cropped?.setValue(slider4.value, forKey: "inputEV")
     
         let originalImage2CG = CIImage(cgImage: (image?.cgImage)!)
         if let output = Cropped?.outputImage
         {
             if let cgimg = context.createCGImage(output, from: originalImage2CG.extent)
             {
                 let processedImage = UIImage(cgImage: cgimg)
               
                 return processedImage
             }
         }
         return image!
     }
     
     func gamma() -> UIImage
     {

         var image:UIImage?
         image = filteredImage
        
         let ciImage = CIImage(image: image!)
         
         let Cropped = CIFilter(name: "CIGammaAdjust")
         Cropped?.setDefaults()
         Cropped?.setValue(ciImage, forKey: "inputImage")
         Cropped?.setValue(slider3.value, forKey: "inputPower")
        
         let originalImage2CG = CIImage(cgImage: (image?.cgImage)!)
         if let output = Cropped?.outputImage
         {
             if let cgimg = context.createCGImage(output, from: originalImage2CG.extent)
             {
                 let processedImage = UIImage(cgImage: cgimg)
               
                 return processedImage
             }
         }
         return image!
     }
    
    @objc func rotationGesture(sender:UIRotationGestureRecognizer)
    {
        print("sender View: \(sender.view)")
       // print("Rotation: \(sender.rotation)")
       // print("before: \(imgView.transform)")
        print("rotation gesture call")
        sender.view?.transform = (sender.view?.transform)!.rotated(by: sender.rotation)
        // print("after 1: \(imgView.transform)")
        sender.rotation = 0
        // print("after 2: \(imgView.transform)")
    }
    
    @objc func panGesture(recognizer: UIPanGestureRecognizer)
    {
        
        
        let transalation = recognizer.translation(in: self.imageFrameUiView)
        if let view = recognizer.view
        {
            view.center = CGPoint(x:view.center.x + transalation.x,y:view.center.y+transalation.y)
        }
        
        recognizer.setTranslation(CGPoint.zero, in: self.imageFrameUiView)
        
    }
    
    @objc func pinchGesture(sender:UIPinchGestureRecognizer)
    {
       // print("before imageview: \(imgView.frame)")
        sender.view?.transform = sender.view!.transform.scaledBy(x: sender.scale,y: sender.scale)
        sender.scale = 1.0
       // print("after imageview: \(imgView.frame)")
        
    }
    
   private func setupCollectionView(){
       collectionView.delegate = self
       collectionView.dataSource = self
    filterCollectionView.delegate = self
    filterCollectionView.dataSource = self
       
       let nib1 = UINib(nibName: "itemCollectionViewCell", bundle: nil)
     
       collectionView.register(nib1, forCellWithReuseIdentifier: cellIdentifier)
    
    
    let nib2 = UINib(nibName: "filterCollectionViewCell", bundle: nil)
        
         filterCollectionView.register(nib2, forCellWithReuseIdentifier: cellIdentifier2)
     
   }
   
   
   
   private func setupCollectionViewItemSize(){
       if (collectionViewFlowLayout == nil){
           
           collectionViewFlowLayout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
           collectionView.setCollectionViewLayout(collectionViewFlowLayout, animated: true)
           
       }
     else if (collectionViewFlowLayout2 == nil){
         
         collectionViewFlowLayout2.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
         filterCollectionView.setCollectionViewLayout(collectionViewFlowLayout2, animated: true)
         
     }
       
       
   }

    
    func takeScreenshot(view: UIView){
   
        self.cropeImage = view.makeSnapshot()
    
    }
    
    
    
    
    
    
 
    
    @IBAction func clickSave(_ sender: Any) {
       takeScreenshot( view: self.imageFrameUiView)
        print("saved pic")
        var img: UIImage?
        img = self.cropeImage
        
        if self.originalImageWidth > self.originalImageHeight
        {
            img = self.cropeImage?.resize(withWidth: self.originalImageWidth)
        }
        else if self.originalImageWidth < self.originalImageHeight
        {
            img = self.cropeImage?.resize(withHeight: self.originalImageHeight)
        }
        else
        {
             img = self.cropeImage?.resize(withHeight: self.originalImageHeight)
        }
        print("after cropping image size: \(img?.size)")
       // img = self.imgView.image
        let activityController = UIActivityViewController(activityItems: [img!], applicationActivities: nil)
        activityController.completionWithItemsHandler = {
            (nil,completed,_,error)in
            if completed{
                print("completed")
            }else
            {
                print("cancel")
            }
        }
        present(activityController, animated: true)
        {
            print("presented")
        }
 
    }
    
     func clickSize1() {
 
        resetSize1()
        resetSize1()
     
        
    }
    
    func resetSize1()
    {
      //  var space = getSpaceSize()
       self.imgView.frame.size.width = self.imageFrameUiView.frame.size.width
       self.imgView.frame.size.height = self.imageFrameUiView.frame.size.height
       self.imgView.frame.origin.x = 0
       self.imgView.frame.origin.y = 0
       self.imgView.contentMode = .scaleAspectFit
       self.imgView.transform =  CGAffineTransform.identity
               
              // self.imgView.frame =  self.tempImageView.frame
               //self.imgView.transform =  self.tempImageView.transform
       self.imgView.image = self.image
    }
    
    func resetSize3()
    {
        resetSize1()
       var space = getBorderAreaSpaceSize()
         imgViewSizeChange()
     /*  self.imgView.frame.size.width = self.imageFrameUiView.frame.size.width - (space+space)
       self.imgView.frame.size.height = self.imageFrameUiView.frame.size.height - (space+space)
       self.imgView.frame.origin.x = space
       self.imgView.frame.origin.y = space
        */
        self.imgView.frame.size.width = self.imgView.frame.size.width - (space+space)
        self.imgView.frame.size.height = self.imgView.frame.size.height - (space+space)
        self.imgView.frame.origin.x =  self.imgView.frame.origin.x + space
        self.imgView.frame.origin.y = self.imgView.frame.origin.y + space
     
      /* if ((self.image?.size.width)! > (self.image?.size.height)!)
       {
             print("width big")
         /*   self.imgView.frame.size.width = self.imageFrameUiView.frame.size.width - (space+space)
          
           self.imgView.frame.size.height = self.imageFrameUiView.frame.size.height
            self.imgView.frame.origin.x = space
        */
           self.imgView.frame.size.width = self.imageFrameUiView.frame.size.width - (space+space)
                     self.imgView.frame.size.height = self.imageFrameUiView.frame.size.height - (space+space)
                     self.imgView.frame.origin.x = space
                     self.imgView.frame.origin.y = space
       }
       else if ((self.image?.size.width)! < (self.image?.size.height)!)
       {
              print("height big")
             self.imgView.frame.size.height = self.imageFrameUiView.frame.size.height - (space+space)
             self.imgView.frame.size.width = self.imageFrameUiView.frame.size.width
             self.imgView.frame.origin.y = space
       }
       else
       {
            print("same size")
           self.imgView.frame.size.width = self.imageFrameUiView.frame.size.width - (space+space)
           self.imgView.frame.size.height = self.imageFrameUiView.frame.size.height - (space+space)
           self.imgView.frame.origin.x = space
           self.imgView.frame.origin.y = space
       }
      */
       self.imgView.contentMode = .scaleAspectFit
      
       self.imgView.image = self.image
    }
    
    
    
    func clickSize2() {
        resetSize1()
    //    var space = getSpaceSize()
        self.imgView.frame.size.width = self.imageFrameUiView.frame.size.width
        self.imgView.frame.size.height = self.imageFrameUiView.frame.size.height
        self.imgView.frame.origin.x = 0
        self.imgView.frame.origin.y = 0
        self.imgView.contentMode = .scaleAspectFill
     
        self.imgView.image = self.image
    }
    
    
   
    
    func clickSize3() {
        resetSize1()
        var space = getBorderAreaSpaceSize()
         imgViewSizeChange()
        /*self.imgView.frame.size.width = self.imageFrameUiView.frame.size.width - (space+space)
        self.imgView.frame.size.height = self.imageFrameUiView.frame.size.height - (space+space)
        self.imgView.frame.origin.x = space
        self.imgView.frame.origin.y = space
        */
        self.imgView.frame.size.width = self.imgView.frame.size.width - (space+space)
        self.imgView.frame.size.height = self.imgView.frame.size.height - (space+space)
        self.imgView.frame.origin.x =  self.imgView.frame.origin.x + space
        self.imgView.frame.origin.y = self.imgView.frame.origin.y + space
    /*    if ((self.image?.size.width)! > (self.image?.size.height)!)
        {
            print("width big")
            print("space: \(space)")
          /*   self.imgView.frame.size.width = self.imageFrameUiView.frame.size.width - (space+space)
           
            self.imgView.frame.size.height = self.imageFrameUiView.frame.size.height
             self.imgView.frame.origin.x = space
          */
            
            self.imgView.frame.size.width = self.imageFrameUiView.frame.size.width - (space+space)
                      self.imgView.frame.size.height = self.imageFrameUiView.frame.size.height - (space+space)
                      self.imgView.frame.origin.x = space
                      self.imgView.frame.origin.y = space
        }
        else if ((self.image?.size.width)! < (self.image?.size.height)!)
        {
               print("height big")
              self.imgView.frame.size.height = self.imageFrameUiView.frame.size.height - (space+space)
              self.imgView.frame.size.width = self.imageFrameUiView.frame.size.width
              self.imgView.frame.origin.y = space
        }
        else
        {
             print("same size")
            self.imgView.frame.size.width = self.imageFrameUiView.frame.size.width - (space+space)
            self.imgView.frame.size.height = self.imageFrameUiView.frame.size.height - (space+space)
            self.imgView.frame.origin.x = space
            self.imgView.frame.origin.y = space
        }
       
        */
        self.imgView.contentMode = .scaleAspectFit
        self.imgView.image = self.image
         resetSize3()
        
    }
    
    func getBorderAreaSpaceSize()->CGFloat
    {
        var w =  (self.imageFrameUiView.frame.size.width + self.imageFrameUiView.frame.height)/2
       var space = (w * 3)/100
       return CGFloat(space)
    }
    
     func pickImage() {
        
        imagePicker.sourceType = .photoLibrary
        imagePicker.allowsEditing = false
        present(imagePicker, animated: true, completion: nil)
    }
    
    
    func applyEffectFilter(processingImage : UIImage,currentFilterName:String)->UIImage
    {
         var filter : CIFilter?
        let ciImage = CIImage(image: processingImage)
        var imageSize = processingImage.size
        
              if currentFilterName == "HDR"
              {
                  
                  filter = CIFilter(name: "CIUnsharpMask")
                  filter?.setDefaults()
                  filter?.setValue(ciImage, forKey: kCIInputImageKey)
                  filter?.setValue(2.50, forKey: "inputRadius")
                  filter?.setValue(0.7, forKey: "inputIntensity")
                  
                  let output2 = filter?.outputImage
                  let cgimg2 = context.createCGImage(output2!, from: output2!.extent)
                  
                  let processedImage2 = UIImage(cgImage: cgimg2!)
                  let ciImage2 = CIImage(image: processedImage2)
                  
                  filter = CIFilter(name: "CIColorControls")
                  filter?.setDefaults()
                  filter?.setValue(ciImage2, forKey: kCIInputImageKey)
                  filter?.setValue(1.07, forKey: "inputSaturation")
                  
                  let output3 = filter?.outputImage
                  let cgimg3 = context.createCGImage(output3!, from: output3!.extent)
                  
                  let processedImage3 = UIImage(cgImage: cgimg3!)
                  let ciImage3 = CIImage(image: processedImage3)
                  
                  filter = CIFilter(name: "CIGaussianBlur")
                  filter?.setDefaults()
                  filter?.setValue(ciImage3, forKey: kCIInputImageKey)
                  filter?.setValue(0.018, forKey: "inputRadius")
                  
                  let output4 = filter?.outputImage
                  let cgimg4 = context.createCGImage(output4!, from: output4!.extent)
                  
                  let processedImage4 = UIImage(cgImage: cgimg4!)
                  let ciImage4 = CIImage(image: processedImage4)
                  
                  filter = CIFilter(name: "CIExposureAdjust")
                  filter?.setDefaults()
                  filter?.setValue(ciImage4, forKey: kCIInputImageKey)
                  filter?.setValue(1, forKey: "inputEV")
                  
                  
                  
              }
              else if currentFilterName == "Dodger"
              {
                  
                  filter = CIFilter(name: "CISepiaTone")
                  filter?.setDefaults()
                  filter?.setValue(ciImage, forKey: kCIInputImageKey)
                  filter?.setValue(0.5, forKey: "inputIntensity")
                  
              }
              else if currentFilterName == "Noise"
              {
                  filter = CIFilter(name: "CIDither")
                  filter?.setDefaults()
                  filter?.setValue(ciImage, forKey: kCIInputImageKey)
                  filter?.setValue(0.15, forKey: "inputIntensity")
                  
              }
              else if currentFilterName == "Film"
              {
                  filter = CIFilter(name: "CIGammaAdjust")
                  filter?.setDefaults()
                  filter?.setValue(ciImage, forKey: kCIInputImageKey)
                  filter?.setValue(1.62, forKey: "inputPower")
                  
                  let output = filter?.outputImage
                  let cgimg = context.createCGImage(output!, from: output!.extent)
                  
                  let processedImage = UIImage(cgImage: cgimg!)
                  let ciImage2 = CIImage(image: processedImage)
                  
                  filter = CIFilter(name: "CIColorControls")
                  filter?.setDefaults()
                  filter?.setValue(ciImage2, forKey: kCIInputImageKey)
                  filter?.setValue(1.3474, forKey: "inputSaturation")
                  filter?.setValue(1, forKey: "inputBrightness")
                  filter?.setValue(2.7185, forKey: "inputContrast")
                  
              }
              else if currentFilterName == "Glitch"
              {
                  filter = CIFilter(name: "CIDisplacementDistortion")
                  filter?.setDefaults()
                  filter?.setValue(ciImage, forKey: kCIInputImageKey)
                  filter?.setValue(ciImage, forKey: "inputDisplacementImage")
                  filter?.setValue(30.514, forKey: "inputScale")
                  
              }
              else if currentFilterName == "GRNG"
                  {
                      filter = CIFilter(name: "CIVibrance")
                      filter?.setDefaults()
                      filter?.setValue(ciImage, forKey: kCIInputImageKey)
                      filter?.setValue(1.0, forKey: "inputAmount")
                      
                      let output = filter?.outputImage
                      let cgimg = context.createCGImage(output!, from: output!.extent)
                      
                      let processedImage = UIImage(cgImage: cgimg!)
                      let ciImage2 = CIImage(image: processedImage)
                      
                      filter = CIFilter(name: "CIDither")
                      filter?.setDefaults()
                      filter?.setValue(ciImage2, forKey: kCIInputImageKey)
                      filter?.setValue(0.25, forKey: "inputIntensity")
                      
                      
                  }
              else if currentFilterName == "VHS"
              {
                  filter = CIFilter(name: "CIDisplacementDistortion")
                  filter?.setDefaults()
                  filter?.setValue(ciImage, forKey: kCIInputImageKey)
                  filter?.setValue(ciImage, forKey: "inputDisplacementImage")
                  filter?.setValue(30.514, forKey: "inputScale")
                  
                  let output2 = filter?.outputImage
                  let cgimg2 = context.createCGImage(output2!, from: output2!.extent)
                  
                  let processedImage2 = UIImage(cgImage: cgimg2!)
                  let ciImage2 = CIImage(image: processedImage2)
                  
                  filter = CIFilter(name: "CIPhotoEffectMono")
                  filter?.setDefaults()
                  filter?.setValue(ciImage2, forKey: kCIInputImageKey)
                  
                  let output3 = filter?.outputImage
                  let cgimg3 = context.createCGImage(output3!, from: output3!.extent)
                  
                  let processedImage3 = UIImage(cgImage: cgimg3!)
                  let ciImage3 = CIImage(image: processedImage3)
                  
                  filter = CIFilter(name: "CIDither")
                  filter?.setDefaults()
                  filter?.setValue(ciImage3, forKey: kCIInputImageKey)
                  filter?.setValue(0.3, forKey: "inputIntensity")
                  
              }
              else if currentFilterName == "Wave"
              {
                  
                  filter = CIFilter(name: "CIColorPolynomial")
                  filter?.setDefaults()
                  filter?.setValue(ciImage, forKey: kCIInputImageKey)
                  filter?.setValue(CIVector(string: "[0 0.4488 0 0]"), forKey: "inputRedCoefficients")
                  filter?.setValue(CIVector(string: "[0 0.4801 0 0]"), forKey: "inputGreenCoefficients")
                  filter?.setValue(CIVector(string: "[0.1501 0.4403 1 0.031]"), forKey: "inputBlueCoefficients")
                  filter?.setValue(CIVector(string: "[0 1 0 0]"), forKey: "inputAlphaCoefficients")
                  
                  let output2 = filter?.outputImage
                  let cgimg2 = context.createCGImage(output2!, from: output2!.extent)
                  
                  let processedImage2 = UIImage(cgImage: cgimg2!)
                  let ciImage2 = CIImage(image: processedImage2)
                  
                  filter = CIFilter(name: "CIDither")
                  filter?.setDefaults()
                  filter?.setValue(ciImage2, forKey: kCIInputImageKey)
                  filter?.setValue(0.4, forKey: "inputIntensity")
                  
              }
              else if currentFilterName == "VIN 1"
              {
                  
                  filter = CIFilter(name: "CIColorControls")
                  filter?.setDefaults()
                  filter?.setValue(ciImage, forKey: kCIInputImageKey)
                  filter?.setValue(1.05, forKey: "inputSaturation")
                  filter?.setValue(0, forKey: "inputBrightness")
                  filter?.setValue(1.05, forKey: "inputContrast")
                  
                  let output2 = filter?.outputImage
                  let cgimg2 = context.createCGImage(output2!, from: output2!.extent)
                  
                  let processedImage2 = UIImage(cgImage: cgimg2!)
                  let ciImage2 = CIImage(image: processedImage2)
                  
                  filter = CIFilter(name: "CITemperatureAndTint")
                  filter?.setDefaults()
                  filter?.setValue(ciImage2, forKey: kCIInputImageKey)
                  filter?.setValue(CIVector(string: "[5681.75 0]"), forKey: "inputNeutral")
                  filter?.setValue(CIVector(string: "[13000 0]"), forKey: "inputTargetNeutral")
                  
                  
                  
                  
              }
              else if currentFilterName == "VIN 2"
              {
                  
                  filter = CIFilter(name: "CIColorPolynomial")
                  filter?.setDefaults()
                  filter?.setValue(ciImage, forKey: kCIInputImageKey)
                  filter?.setValue(CIVector(string: "[0.13 1 0 0]"), forKey: "inputRedCoefficients")
                  filter?.setValue(CIVector(string: "[0.01 1 0 0]"), forKey: "inputGreenCoefficients")
                  filter?.setValue(CIVector(string: "[0.07 1 0 0]"), forKey: "inputBlueCoefficients")
                  filter?.setValue(CIVector(string: "[0 1 0 0]"), forKey: "inputAlphaCoefficients")
                  
                  
                  let output2 = filter?.outputImage
                  let cgimg2 = context.createCGImage(output2!, from: output2!.extent)
                  
                  let processedImage2 = UIImage(cgImage: cgimg2!)
                  let ciImage2 = CIImage(image: processedImage2)
                  
                  filter = CIFilter(name: "CIColorControls")
                  filter?.setDefaults()
                  filter?.setValue(ciImage2, forKey: kCIInputImageKey)
                  filter?.setValue(1.18, forKey: "inputSaturation")
                  filter?.setValue(0.03, forKey: "inputBrightness")
                  filter?.setValue(1.08, forKey: "inputContrast")
                  
                  let output3 = filter?.outputImage
                  let cgimg3 = context.createCGImage(output3!, from: output3!.extent)
                  
                  let processedImage3 = UIImage(cgImage: cgimg3!)
                  let ciImage3 = CIImage(image: processedImage3)
                  
                  filter = CIFilter(name: "CIPhotoEffectFade")
                  filter?.setDefaults()
                  filter?.setValue(ciImage3, forKey: kCIInputImageKey)
                  
                  let output4 = filter?.outputImage
                  let cgimg4 = context.createCGImage(output4!, from: output4!.extent)
                  
                  let processedImage4 = UIImage(cgImage: cgimg4!)
                  let ciImage4 = CIImage(image: processedImage4)
                  
                  filter = CIFilter(name: "CIColorControls")
                  filter?.setDefaults()
                  filter?.setValue(ciImage4, forKey: kCIInputImageKey)
                  filter?.setValue(1.05, forKey: "inputSaturation")
                  filter?.setValue(0, forKey: "inputBrightness")
                  filter?.setValue(1.05, forKey: "inputContrast")
                  
              }
              else if currentFilterName == "VIN 3"
              {
                  
                  filter = CIFilter(name: "CITemperatureAndTint")
                  filter?.setDefaults()
                  filter?.setValue(ciImage, forKey: kCIInputImageKey)
                  filter?.setValue(CIVector(string: "[5000 0]"), forKey: "inputNeutral")
                  filter?.setValue(CIVector(string: "[5773.69 0]"), forKey: "inputTargetNeutral")
                  
                  let output2 = filter?.outputImage
                  let cgimg2 = context.createCGImage(output2!, from: output2!.extent)
                  
                  let processedImage2 = UIImage(cgImage: cgimg2!)
                  let ciImage2 = CIImage(image: processedImage2)
                  
                  filter = CIFilter(name: "CIColorControls")
                  filter?.setDefaults()
                  filter?.setValue(ciImage2, forKey: kCIInputImageKey)
                  filter?.setValue(1, forKey: "inputSaturation")
                  filter?.setValue(0.035, forKey: "inputBrightness")
                  filter?.setValue(1, forKey: "inputContrast")
                  
                  let output3 = filter?.outputImage
                  let cgimg3 = context.createCGImage(output3!, from: output3!.extent)
                  
                  let processedImage3 = UIImage(cgImage: cgimg3!)
                  let ciImage3 = CIImage(image: processedImage3)
                  
                  
                  filter = CIFilter(name: "CIGammaAdjust")
                  filter?.setDefaults()
                  filter?.setValue(ciImage3, forKey: kCIInputImageKey)
                  filter?.setValue(1, forKey: "inputPower")
                  
                  
              }
              else if currentFilterName == "VIN 4"
              {
                  //reddish blue
                  filter = CIFilter(name: "CIPhotoEffectProcess")
                  filter?.setDefaults()
                  filter?.setValue(ciImage, forKey: kCIInputImageKey)
                  
                  let output2 = filter?.outputImage
                  let cgimg2 = context.createCGImage(output2!, from: output2!.extent)
                  
                  let processedImage2 = UIImage(cgImage: cgimg2!)
                  let ciImage2 = CIImage(image: processedImage2)
                  
                  
                  filter = CIFilter(name: "CIColorControls")
                  filter?.setDefaults()
                  filter?.setValue(ciImage2, forKey: kCIInputImageKey)
                  filter?.setValue(1, forKey: "inputSaturation")
                  filter?.setValue(0, forKey: "inputBrightness")
                  filter?.setValue(1.1, forKey: "inputContrast")
                  
              }
              else if currentFilterName == "MIL"
              {
                  
                  filter = CIFilter(name: "CIColorControls")
                  filter?.setDefaults()
                  filter?.setValue(ciImage, forKey: kCIInputImageKey)
                  filter?.setValue(1.15, forKey: "inputSaturation")
                  filter?.setValue(0.05, forKey: "inputBrightness")
                  filter?.setValue(1.1, forKey: "inputContrast")
                  
              }
              else if currentFilterName == "MIL 2"
              {
                  
                  filter = CIFilter(name: "CIColorControls")
                  filter?.setDefaults()
                  filter?.setValue(ciImage, forKey: kCIInputImageKey)
                  filter?.setValue(1.2, forKey: "inputSaturation")
                  filter?.setValue(0.045, forKey: "inputBrightness")
                  filter?.setValue(1.13, forKey: "inputContrast")
                  
                  let output2 = filter?.outputImage
                  let cgimg2 = context.createCGImage(output2!, from: output2!.extent)
                  
                  let processedImage2 = UIImage(cgImage: cgimg2!)
                  let ciImage2 = CIImage(image: processedImage2)
                  
                  
                  filter = CIFilter(name: "CIGammaAdjust")
                  filter?.setDefaults()
                  filter?.setValue(ciImage2, forKey: kCIInputImageKey)
                  filter?.setValue(1.135, forKey: "inputPower")
                  
                  let output3 = filter?.outputImage
                  let cgimg3 = context.createCGImage(output3!, from: output3!.extent)
                  
                  let processedImage3 = UIImage(cgImage: cgimg3!)
                  let ciImage3 = CIImage(image: processedImage3)
                  
                  
                  filter = CIFilter(name: "CISepiaTone")
                  filter?.setDefaults()
                  filter?.setValue(ciImage3, forKey: kCIInputImageKey)
                  filter?.setValue(0.5, forKey: "inputIntensity")
                  
                  
              }
              else if currentFilterName == "MIL 3"
              {
                  filter = CIFilter(name: "CISepiaTone")
                  filter?.setDefaults()
                  filter?.setValue(ciImage, forKey: kCIInputImageKey)
                  filter?.setValue(0.1, forKey: "inputIntensity")
                  
                  let output2 = filter?.outputImage
                  let cgimg2 = context.createCGImage(output2!, from: output2!.extent)
                  
                  let processedImage2 = UIImage(cgImage: cgimg2!)
                  let ciImage2 = CIImage(image: processedImage2)
                  
                  filter = CIFilter(name: "CIColorControls")
                  filter?.setDefaults()
                  filter?.setValue(ciImage2, forKey: kCIInputImageKey)
                  filter?.setValue(1.1911, forKey: "inputSaturation")
                  filter?.setValue(0.18, forKey: "inputBrightness")
                  filter?.setValue(1.36, forKey: "inputContrast")
                  
              }
              else if currentFilterName == "MIL 4"
              {
                  
                  filter = CIFilter(name: "CISepiaTone")
                  filter?.setDefaults()
                  filter?.setValue(ciImage, forKey: kCIInputImageKey)
                  filter?.setValue(0.5, forKey: "inputIntensity")
                  
                  let output2 = filter?.outputImage
                  let cgimg2 = context.createCGImage(output2!, from: output2!.extent)
                  
                  let processedImage2 = UIImage(cgImage: cgimg2!)
                  let ciImage2 = CIImage(image: processedImage2)
                  
                  filter = CIFilter(name: "CIColorControls")
                  filter?.setDefaults()
                  filter?.setValue(ciImage2, forKey: kCIInputImageKey)
                  filter?.setValue(1.3, forKey: "inputSaturation")
                  filter?.setValue(0.2, forKey: "inputBrightness")
                  filter?.setValue(1.42, forKey: "inputContrast")
                  
              }
             else if currentFilterName == "Seafoam"
                {
                    
                    filter = CIFilter(name: "CIGammaAdjust")
                    filter?.setDefaults()
                    filter?.setValue(ciImage, forKey: kCIInputImageKey)
                    filter?.setValue(1, forKey: "inputPower")
                    
                    let output2 = filter?.outputImage
                    let cgimg2 = context.createCGImage(output2!, from: output2!.extent)
                    
                    let processedImage2 = UIImage(cgImage: cgimg2!)
                    let ciImage2 = CIImage(image: processedImage2)
                    
                    filter = CIFilter(name: "CILinearToSRGBToneCurve")
                    filter?.setDefaults()
                    filter?.setValue(ciImage2, forKey: kCIInputImageKey)
                    
                    let output3 = filter?.outputImage
                    let cgimg3 = context.createCGImage(output3!, from: output3!.extent)
                    
                    let processedImage3 = UIImage(cgImage: cgimg3!)
                    let ciImage3 = CIImage(image: processedImage3)
                    
                    filter = CIFilter(name: "CIColorControls")
                    filter?.setDefaults()
                    filter?.setValue(ciImage3, forKey: kCIInputImageKey)
                    filter?.setValue(1.1, forKey: "inputSaturation")
                    filter?.setValue(0, forKey: "inputBrightness")
                    filter?.setValue(1.35, forKey: "inputContrast")
                    
                    
                }
                else if currentFilterName == "Drama"
                {
                    
                    filter = CIFilter(name: "CIColorControls")
                    filter?.setDefaults()
                    filter?.setValue(ciImage, forKey: kCIInputImageKey)
                    filter?.setValue(0.03, forKey: "inputBrightness")
                    
                    let output2 = filter?.outputImage
                    let cgimg2 = context.createCGImage(output2!, from: output2!.extent)
                    
                    let processedImage2 = UIImage(cgImage: cgimg2!)
                    let ciImage2 = CIImage(image: processedImage2)
                    
                    filter = CIFilter(name: "CIWhitePointAdjust")
                    filter?.setDefaults()
                    filter?.setValue(ciImage2, forKey: kCIInputImageKey)
                    filter?.setValue(CIColor(string: "1110.6306"), forKey: "inputColor")
                    
                    
                    let output3 = filter?.outputImage
                    let cgimg3 = context.createCGImage(output3!, from: output3!.extent)
                    //
                    let processedImage3 = UIImage(cgImage: cgimg3!)
                    let ciImage3 = CIImage(image: processedImage3)
                    
                    filter = CIFilter(name: "CIUnsharpMask")
                    filter?.setDefaults()
                    filter?.setValue(ciImage3, forKey: kCIInputImageKey)
                    filter?.setValue(2.00, forKey: "inputRadius")
                    filter?.setValue(2.389, forKey: "inputIntensity")
                    
                    
                }
                else if currentFilterName == "B/W"
                {
                    
                    filter = CIFilter(name: "CIPhotoEffectMono")
                    filter?.setDefaults()
                    filter?.setValue(ciImage, forKey: kCIInputImageKey)
                    
                    let output2 = filter?.outputImage
                    let cgimg2 = context.createCGImage(output2!, from: output2!.extent)
                    
                    let processedImage2 = UIImage(cgImage: cgimg2!)
                    let ciImage2 = CIImage(image: processedImage2)
                    
                    filter = CIFilter(name: "CIColorControls")
                    filter?.setDefaults()
                    filter?.setValue(ciImage2, forKey: kCIInputImageKey)
                    filter?.setValue(-0.01, forKey: "inputBrightness")
                    
                    
                }
                else if currentFilterName == "B/W Hicon"
                {
                    
                    filter = CIFilter(name: "CIMinimumComponent")
                    filter?.setDefaults()
                    filter?.setValue(ciImage, forKey: kCIInputImageKey)
                    
                }
                else if currentFilterName == "Film B/W"
                {
                    
                    
                    filter = CIFilter(name: "CIPhotoEffectMono")
                    filter?.setDefaults()
                    filter?.setValue(ciImage, forKey: kCIInputImageKey)
                    
                    let output2 = filter?.outputImage
                    let cgimg2 = context.createCGImage(output2!, from: output2!.extent)
                    
                    let processedImage2 = UIImage(cgImage: cgimg2!)
                    let ciImage2 = CIImage(image: processedImage2)
                    
                    filter = CIFilter(name: "CIColorControls")
                    filter?.setDefaults()
                    filter?.setValue(ciImage2, forKey: kCIInputImageKey)
                    filter?.setValue(0.8, forKey: "inputSaturation")
                    filter?.setValue(0.85, forKey: "inputBrightness")
                    filter?.setValue(3, forKey: "inputContrast")
                    
                    let output3 = filter?.outputImage
                    let cgimg3 = context.createCGImage(output3!, from: output3!.extent)
                    
                    let processedImage3 = UIImage(cgImage: cgimg3!)
                    let ciImage3 = CIImage(image: processedImage3)
                    
                    filter = CIFilter(name: "CIGammaAdjust")
                    filter?.setDefaults()
                    filter?.setValue(ciImage3, forKey: kCIInputImageKey)
                    filter?.setValue(1, forKey: "inputPower")
                    
                    let output4 = filter?.outputImage
                    let cgimg4 = context.createCGImage(output4!, from: output4!.extent)
                    
                    let processedImage4 = UIImage(cgImage: cgimg4!)
                    let ciImage4 = CIImage(image: processedImage4)
                    
                    filter = CIFilter(name: "CIColorClamp")
                    filter?.setDefaults()
                    filter?.setValue(ciImage4, forKey: kCIInputImageKey)
                    filter?.setValue(CIVector(string: "[0 0 0 0]"), forKey: "inputMinComponents")
                    filter?.setValue(CIVector(string: "[1 1 1 0.6]"), forKey: "inputMaxComponents")
                    
                    let output5 = filter?.outputImage
                    let cgimg5 = context.createCGImage(output5!, from: output5!.extent)
                    
                    let processedImage5 = UIImage(cgImage: cgimg5!)
                    let ciImage5 = CIImage(image: processedImage5)
                    
                    filter = CIFilter(name: "CIColorControls")
                    filter?.setDefaults()
                    filter?.setValue(ciImage5, forKey: kCIInputImageKey)
                    filter?.setValue(0.25, forKey: "inputBrightness")
                    
                    
                    
                }
                else if currentFilterName == "MV3"
                {
                    
                    filter = CIFilter(name: "CIColorControls")
                    filter?.setDefaults()
                    filter?.setValue(ciImage, forKey: kCIInputImageKey)
                    filter?.setValue(1.3, forKey: "inputSaturation")
                    
                    
                }
                else if currentFilterName == "B/W Lowcon"
                {
                    
                    filter = CIFilter(name: "CIColorControls")
                    filter?.setDefaults()
                    filter?.setValue(ciImage, forKey: kCIInputImageKey)
                    filter?.setValue(0, forKey: "inputSaturation")
                    filter?.setValue(0.5, forKey: "inputBrightness")
                    filter?.setValue(1.9, forKey: "inputContrast")
                    
                    let output2 = filter?.outputImage
                    let cgimg2 = context.createCGImage(output2!, from: output2!.extent)
                    
                    let processedImage2 = UIImage(cgImage: cgimg2!)
                    let ciImage2 = CIImage(image: processedImage2)
                    
                    filter = CIFilter(name: "CISepiaTone")
                    filter?.setDefaults()
                    filter?.setValue(ciImage2, forKey: kCIInputImageKey)
                    filter?.setValue(0.9, forKey: "inputIntensity")
                    
                    let output3 = filter?.outputImage
                    let cgimg3 = context.createCGImage(output3!, from: output3!.extent)
                    
                    let processedImage3 = UIImage(cgImage: cgimg3!)
                    let ciImage3 = CIImage(image: processedImage3)
                    
                    filter = CIFilter(name: "CIPhotoEffectNoir")
                    filter?.setDefaults()
                    filter?.setValue(ciImage3, forKey: kCIInputImageKey)
                    
                }
                else if currentFilterName == "MV4"
                {
                    
                    filter = CIFilter(name: "CISepiaTone")
                    filter?.setDefaults()
                    filter?.setValue(ciImage, forKey: kCIInputImageKey)
                    filter?.setValue(0.2, forKey: "inputIntensity")
                    
                    let output2 = filter?.outputImage
                    let cgimg2 = context.createCGImage(output2!, from: output2!.extent)
                    
                    let processedImage2 = UIImage(cgImage: cgimg2!)
                    let ciImage2 = CIImage(image: processedImage2)
                    
                    filter = CIFilter(name: "CIColorControls")
                    filter?.setDefaults()
                    filter?.setValue(ciImage2, forKey: kCIInputImageKey)
                    filter?.setValue(1.12, forKey: "inputSaturation")
                    filter?.setValue(0.65, forKey: "inputBrightness")
                    filter?.setValue(2.4, forKey: "inputContrast")
                    
                }
                else if currentFilterName == "Cinerama"
                {
                    
                    filter = CIFilter(name: "CIColorControls")
                    filter?.setDefaults()
                    filter?.setValue(ciImage, forKey: kCIInputImageKey)
                    filter?.setValue(0.89, forKey: "inputSaturation")
                    filter?.setValue(0.62, forKey: "inputBrightness")
                    filter?.setValue(2.26, forKey: "inputContrast")
                    
                    let output2 = filter?.outputImage
                    let cgimg2 = context.createCGImage(output2!, from: output2!.extent)
                    
                    let processedImage2 = UIImage(cgImage: cgimg2!)
                    let ciImage2 = CIImage(image: processedImage2)
                    
                    filter = CIFilter(name: "CIGaussianBlur")
                    filter?.setDefaults()
                    filter?.setValue(ciImage2, forKey: kCIInputImageKey)
                    filter?.setValue(0.68, forKey: "inputRadius")
                    
                    let output3 = filter?.outputImage
                    let cgimg3 = context.createCGImage(output3!, from: output3!.extent)
                    
                    let processedImage3 = UIImage(cgImage: cgimg3!)
                    let ciImage3 = CIImage(image: processedImage3)
                    
                    filter = CIFilter(name: "CISepiaTone")
                    filter?.setDefaults()
                    filter?.setValue(ciImage3, forKey: kCIInputImageKey)
                    filter?.setValue(0.03, forKey: "inputIntensity")
                    
                }
                else if currentFilterName == "Ivory"
                {
                    
                    filter = CIFilter(name: "CIColorControls")
                    filter?.setDefaults()
                    filter?.setValue(ciImage, forKey: kCIInputImageKey)
                    filter?.setValue(1.047, forKey: "inputSaturation")
                    filter?.setValue(0.94, forKey: "inputBrightness")
                    filter?.setValue(2.90, forKey: "inputContrast")
                    
                    let output2 = filter?.outputImage
                    let cgimg2 = context.createCGImage(output2!, from: output2!.extent)
                    
                    let processedImage2 = UIImage(cgImage: cgimg2!)
                    let ciImage2 = CIImage(image: processedImage2)
                    
                    filter = CIFilter(name: "CIMedianFilter")
                    filter?.setDefaults()
                    filter?.setValue(ciImage2, forKey: kCIInputImageKey)
                    
                }
                else if currentFilterName == "Warm Color"
                {
                    
                    filter = CIFilter(name: "CIHueAdjust")
                    filter?.setDefaults()
                    filter?.setValue(ciImage, forKey: kCIInputImageKey)
                    filter?.setValue(0.4181, forKey: "inputAngle")
                    
                    let output2 = filter?.outputImage
                    let cgimg2 = context.createCGImage(output2!, from: output2!.extent)
                    
                    let processedImage2 = UIImage(cgImage: cgimg2!)
                    let ciImage2 = CIImage(image: processedImage2)
                    
                    filter = CIFilter(name: "CIColorControls")
                    filter?.setDefaults()
                    filter?.setValue(ciImage2, forKey: kCIInputImageKey)
                    filter?.setValue(1.2, forKey: "inputSaturation")
                    filter?.setValue(0.0585, forKey: "inputBrightness")
                    filter?.setValue(1.1, forKey: "inputContrast")
                    
                    let output3 = filter?.outputImage
                    let cgimg3 = context.createCGImage(output3!, from: output3!.extent)
                    
                    let processedImage3 = UIImage(cgImage: cgimg3!)
                    let ciImage3 = CIImage(image: processedImage3)
                    
                    filter = CIFilter(name: "CISepiaTone")
                    filter?.setDefaults()
                    filter?.setValue(ciImage3, forKey: kCIInputImageKey)
                    filter?.setValue(0.65, forKey: "inputIntensity")
                    
                }
                else if currentFilterName == "Light Cross"
                {
                    
                    filter = CIFilter(name: "CIPhotoEffectProcess")
                    filter?.setDefaults()
                    filter?.setValue(ciImage, forKey: kCIInputImageKey)
                    
                    
                }
                else if currentFilterName == "Sun 1"
                {
                    
                    filter = CIFilter(name: "CIColorControls")
                    filter?.setDefaults()
                    filter?.setValue(ciImage, forKey: kCIInputImageKey)
                    filter?.setValue(1.24, forKey: "inputSaturation")
                    filter?.setValue(0.325, forKey: "inputBrightness")
                    filter?.setValue(1.6941, forKey: "inputContrast")
                    
                    let output2 = filter?.outputImage
                    let cgimg2 = context.createCGImage(output2!, from: output2!.extent)
                    
                    let processedImage2 = UIImage(cgImage: cgimg2!)
                    let ciImage2 = CIImage(image: processedImage2)
                    
                    filter = CIFilter(name: "CISepiaTone")
                    filter?.setDefaults()
                    filter?.setValue(ciImage2, forKey: kCIInputImageKey)
                    filter?.setValue(0.15, forKey: "inputIntensity")
                    
                    
                }
                else if currentFilterName == "Sun 2"
                {
                    
                    filter = CIFilter(name: "CIHueAdjust")
                    filter?.setDefaults()
                    filter?.setValue(ciImage, forKey: kCIInputImageKey)
                    filter?.setValue(0.1939, forKey: "inputAngle")
                    
                    let output2 = filter?.outputImage
                    let cgimg2 = context.createCGImage(output2!, from: output2!.extent)
                    
                    let processedImage2 = UIImage(cgImage: cgimg2!)
                    let ciImage2 = CIImage(image: processedImage2)
                    
                    filter = CIFilter(name: "CISepiaTone")
                    filter?.setDefaults()
                    filter?.setValue(ciImage2, forKey: kCIInputImageKey)
                    filter?.setValue(0.35, forKey: "inputIntensity")
                    
                }
                else if currentFilterName == "Sun 3"
                {
                    
                    filter = CIFilter(name: "CIPhotoEffectTransfer")
                    filter?.setDefaults()
                    filter?.setValue(ciImage, forKey: kCIInputImageKey)
                    
                    
                }
                else if currentFilterName == "Crisp"
                {
                    
                    filter = CIFilter(name: "CIPhotoEffectProcess")
                    filter?.setDefaults()
                    filter?.setValue(ciImage, forKey: kCIInputImageKey)
                    
                    let output2 = filter?.outputImage
                    let cgimg2 = context.createCGImage(output2!, from: output2!.extent)
                    
                    let processedImage2 = UIImage(cgImage: cgimg2!)
                    let ciImage2 = CIImage(image: processedImage2)
                    
                    filter = CIFilter(name: "CIColorControls")
                    filter?.setDefaults()
                    filter?.setValue(ciImage2, forKey: kCIInputImageKey)
                    filter?.setValue(1.24, forKey: "inputSaturation")
                    filter?.setValue(0.28, forKey: "inputBrightness")
                    filter?.setValue(1.6941, forKey: "inputContrast")
                    
                    let output3 = filter?.outputImage
                    let cgimg3 = context.createCGImage(output3!, from: output3!.extent)
                    
                    let processedImage3 = UIImage(cgImage: cgimg3!)
                    let ciImage3 = CIImage(image: processedImage3)
                    
                    filter = CIFilter(name: "CINoiseReduction")
                    filter?.setDefaults()
                    filter?.setValue(ciImage3, forKey: kCIInputImageKey)
                    filter?.setValue(0.02, forKey: "inputNoiseLevel")
                    filter?.setValue(0.80, forKey: "inputSharpness")
                    
                }
                else if currentFilterName == "Vibrant"
                {
                    
                    filter = CIFilter(name: "CIColorControls")
                    filter?.setDefaults()
                    filter?.setValue(ciImage, forKey: kCIInputImageKey)
                    filter?.setValue(1.3, forKey: "inputSaturation")
                    filter?.setValue(0.37, forKey: "inputBrightness")
                    filter?.setValue(1.8, forKey: "inputContrast")
                    
                    let output2 = filter?.outputImage
                    let cgimg2 = context.createCGImage(output2!, from: output2!.extent)
                    
                    let processedImage2 = UIImage(cgImage: cgimg2!)
                    let ciImage2 = CIImage(image: processedImage2)
                    
                    filter = CIFilter(name: "CIColorPolynomial")
                    filter?.setDefaults()
                    filter?.setValue(ciImage2, forKey: kCIInputImageKey)
                    filter?.setValue(CIVector(string: "[0 0.5 0 0]"), forKey: "inputRedCoefficients")
                    filter?.setValue(CIVector(string: "[0 0.5 0 0]"), forKey: "inputGreenCoefficients")
                    filter?.setValue(CIVector(string: "[0 1 0 0]"), forKey: "inputBlueCoefficients")
                    filter?.setValue(CIVector(string: "[0 1 0 0]"), forKey: "inputAlphaCoefficients")
                    
                    let output3 = filter?.outputImage
                    let cgimg3 = context.createCGImage(output3!, from: output3!.extent)
                    
                    let processedImage3 = UIImage(cgImage: cgimg3!)
                    let ciImage3 = CIImage(image: processedImage3)
                    
                    filter = CIFilter(name: "CISepiaTone")
                    filter?.setDefaults()
                    filter?.setValue(ciImage3, forKey: kCIInputImageKey)
                    filter?.setValue(0.6, forKey: "inputIntensity")
                    
                    
                }
                else if currentFilterName == "MNCH1"
                {
                    
                    filter = CIFilter(name: "CIPhotoEffectMono")
                    filter?.setDefaults()
                    filter?.setValue(ciImage, forKey: kCIInputImageKey)
                    
                }
                else if currentFilterName == "MNCH2"
                {
                    
                    filter = CIFilter(name: "CIPhotoEffectNoir")
                    filter?.setDefaults()
                    filter?.setValue(ciImage, forKey: kCIInputImageKey)
                    
                }
                else if currentFilterName == "MNCH3"
                {
                    
                    filter = CIFilter(name: "CIPhotoEffectMono")
                    filter?.setDefaults()
                    filter?.setValue(ciImage, forKey: kCIInputImageKey)
                    
                    let output2 = filter?.outputImage
                    let cgimg2 = context.createCGImage(output2!, from: output2!.extent)
                    
                    let processedImage2 = UIImage(cgImage: cgimg2!)
                    let ciImage2 = CIImage(image: processedImage2)
                    
                    filter = CIFilter(name: "CIColorControls")
                    filter?.setDefaults()
                    filter?.setValue(ciImage2, forKey: kCIInputImageKey)
                    filter?.setValue(0.02, forKey: "inputBrightness")
                    
                    
                }
                else if currentFilterName == "BLH1"
                {
                    
                    filter = CIFilter(name: "CIExposureAdjust")
                    filter?.setDefaults()
                    filter?.setValue(ciImage, forKey: kCIInputImageKey)
                    filter?.setValue(1.8088, forKey: "inputEV")
                    
                    
                    let output2 = filter?.outputImage
                    let cgimg2 = context.createCGImage(output2!, from: output2!.extent)
                    
                    let processedImage2 = UIImage(cgImage: cgimg2!)
                    let ciImage2 = CIImage(image: processedImage2)
                    
                    filter = CIFilter(name: "CIColorControls")
                    filter?.setDefaults()
                    filter?.setValue(ciImage2, forKey: kCIInputImageKey)
                    filter?.setValue(0.03, forKey: "inputBrightness")
                    
                    
                }
            else if currentFilterName == "BLH2"
            {
                
                filter = CIFilter(name: "CIExposureAdjust")
                filter?.setDefaults()
                filter?.setValue(ciImage, forKey: kCIInputImageKey)
                filter?.setValue(1.875, forKey: "inputEV")
                
                
            }
            else if currentFilterName == "BLH3"
            {
                
                filter = CIFilter(name: "CIColorControls")
                filter?.setDefaults()
                filter?.setValue(ciImage, forKey: kCIInputImageKey)
                filter?.setValue(0.95, forKey: "inputSaturation")
                filter?.setValue(0.5349, forKey: "inputBrightness")
                filter?.setValue(2.12, forKey: "inputContrast")
                
            }
            else if currentFilterName == "Retro"
            {
                
                filter = CIFilter(name: "CIColorControls")
                filter?.setDefaults()
                filter?.setValue(ciImage, forKey: kCIInputImageKey)
                filter?.setValue(1.022, forKey: "inputSaturation")
                filter?.setValue(0.512, forKey: "inputBrightness")
                filter?.setValue(2.1, forKey: "inputContrast")
                
                let output2 = filter?.outputImage
                let cgimg2 = context.createCGImage(output2!, from: output2!.extent)
                
                let processedImage2 = UIImage(cgImage: cgimg2!)
                let ciImage2 = CIImage(image: processedImage2)
                
                filter = CIFilter(name: "CISepiaTone")
                filter?.setDefaults()
                filter?.setValue(ciImage2, forKey: kCIInputImageKey)
                filter?.setValue(0.15, forKey: "inputIntensity")
                
            }
            else if currentFilterName == "Sunny"
            {
                filter = CIFilter(name: "CIColorPolynomial")
                filter?.setDefaults()
                filter?.setValue(ciImage, forKey: kCIInputImageKey)
                filter?.setValue(CIVector(string: "[0 0.6335 0 0.653409]"), forKey: "inputRedCoefficients")
                filter?.setValue(CIVector(string: "[0 0.8437 0.5779 0.2244]"), forKey: "inputGreenCoefficients")
                filter?.setValue(CIVector(string: "[0 0.4573 0 0.2869]"), forKey: "inputBlueCoefficients")
                filter?.setValue(CIVector(string: "[0 0.1477 0.3512 1]"), forKey: "inputAlphaCoefficients")
                
                
            }
            else if currentFilterName == "KPP1"
            {
                
                filter = CIFilter(name: "CIColorControls")
                filter?.setDefaults()
                filter?.setValue(ciImage, forKey: kCIInputImageKey)
                filter?.setValue(0.819, forKey: "inputSaturation")
                filter?.setValue(0.832, forKey: "inputBrightness")
                filter?.setValue(2.762, forKey: "inputContrast")
                
            }
            else if currentFilterName == "KPP2"
            {
                
                filter = CIFilter(name: "CIColorControls")
                filter?.setDefaults()
                filter?.setValue(ciImage, forKey: kCIInputImageKey)
                filter?.setValue(1.005, forKey: "inputSaturation")
                filter?.setValue(0.492, forKey: "inputBrightness")
                filter?.setValue(1.983, forKey: "inputContrast")
                
            }
            else if currentFilterName == "KPP3"
            {
                
                filter = CIFilter(name: "CIColorControls")
                filter?.setDefaults()
                filter?.setValue(ciImage, forKey: kCIInputImageKey)
                filter?.setValue(0.985, forKey: "inputSaturation")
                filter?.setValue(0.540, forKey: "inputBrightness")
                filter?.setValue(2.118, forKey: "inputContrast")
                
            }
            else if currentFilterName == "Vintage"
            {
                
                
                filter = CIFilter(name: "CIColorMatrix")
                filter?.setDefaults()
                filter?.setValue(ciImage, forKey: kCIInputImageKey)
                filter?.setValue(CIVector(string: "[0 1 1 0]"), forKey: "inputRVector")
                filter?.setValue(CIVector(string: "[0.27 1 1 0.13]"), forKey: "inputGVector")
                filter?.setValue(CIVector(string: "[0.19 0.48 0.35 0.21]"), forKey: "inputBVector")
                filter?.setValue(CIVector(string: "[0.15 0.32 0 0.59375]"), forKey: "inputAVector")
                filter?.setValue(CIVector(string: "[0 0.045 0 0]"), forKey: "inputBiasVector")
                
                
            }
            else if currentFilterName == "Warm Amber"
            {
                
                filter = CIFilter(name: "CISepiaTone")
                filter?.setDefaults()
                filter?.setValue(ciImage, forKey: kCIInputImageKey)
                filter?.setValue(0.6, forKey: "inputIntensity")
                
                let output2 = filter?.outputImage
                let cgimg2 = context.createCGImage(output2!, from: output2!.extent)
                
                let processedImage2 = UIImage(cgImage: cgimg2!)
                let ciImage2 = CIImage(image: processedImage2)
                
                
                
                filter = CIFilter(name: "CIColorControls")
                filter?.setDefaults()
                filter?.setValue(ciImage2, forKey: kCIInputImageKey)
                filter?.setValue(1.4, forKey: "inputSaturation")
                filter?.setValue(0.03, forKey: "inputBrightness")
                filter?.setValue(1, forKey: "inputContrast")
                
                
                
            }
            else if currentFilterName == "Sharpen"
            {
                
                filter = CIFilter(name: "CIUnsharpMask")
                filter?.setDefaults()
                filter?.setValue(ciImage, forKey: kCIInputImageKey)
                filter?.setValue(2.169, forKey: "inputRadius")
                filter?.setValue(1, forKey: "inputIntensity")
                
            }
            else if currentFilterName == "HDR2"
            {
                
                filter = CIFilter(name: "CIUnsharpMask")
                filter?.setDefaults()
                filter?.setValue(ciImage, forKey: kCIInputImageKey)
                filter?.setValue(2.50, forKey: "inputRadius")
                filter?.setValue(0.7, forKey: "inputIntensity")
                
                let output2 = filter?.outputImage
                let cgimg2 = context.createCGImage(output2!, from: output2!.extent)
                
                let processedImage2 = UIImage(cgImage: cgimg2!)
                let ciImage2 = CIImage(image: processedImage2)
                
                filter = CIFilter(name: "CIColorControls")
                filter?.setDefaults()
                filter?.setValue(ciImage2, forKey: kCIInputImageKey)
                filter?.setValue(1.1, forKey: "inputSaturation")
                filter?.setValue(0.097, forKey: "inputBrightness")
                filter?.setValue(1.241242, forKey: "inputContrast")
                
                let output3 = filter?.outputImage
                let cgimg3 = context.createCGImage(output3!, from: output3!.extent)
                
                let processedImage3 = UIImage(cgImage: cgimg3!)
                let ciImage3 = CIImage(image: processedImage3)
                
                filter = CIFilter(name: "CIGaussianBlur")
                filter?.setDefaults()
                filter?.setValue(ciImage3, forKey: kCIInputImageKey)
                filter?.setValue(0.06, forKey: "inputRadius")
                
                
            }
            else if currentFilterName == "B/W HDR"
            {
                
                filter = CIFilter(name: "CIPhotoEffectNoir")
                filter?.setDefaults()
                filter?.setValue(ciImage, forKey: kCIInputImageKey)
                
                let output2 = filter?.outputImage
                let cgimg2 = context.createCGImage(output2!, from: output2!.extent)
                
                let processedImage2 = UIImage(cgImage: cgimg2!)
                let ciImage2 = CIImage(image: processedImage2)
                
                filter = CIFilter(name: "CIUnsharpMask")
                filter?.setDefaults()
                filter?.setValue(ciImage2, forKey: kCIInputImageKey)
                filter?.setValue(2.169, forKey: "inputRadius")
                filter?.setValue(1, forKey: "inputIntensity")
                
                let output3 = filter?.outputImage
                let cgimg3 = context.createCGImage(output3!, from: output3!.extent)
                
                let processedImage3 = UIImage(cgImage: cgimg3!)
                let ciImage3 = CIImage(image: processedImage3)
                
                filter = CIFilter(name: "CIGaussianBlur")
                filter?.setDefaults()
                filter?.setValue(ciImage3, forKey: kCIInputImageKey)
                filter?.setValue(0.06, forKey: "inputRadius")
                
            }
            else if currentFilterName == "MV1"
            {
                
                filter = CIFilter(name: "CIColorControls")
                filter?.setDefaults()
                filter?.setValue(ciImage, forKey: kCIInputImageKey)
                filter?.setValue(1.202, forKey: "inputSaturation")
                filter?.setValue(1, forKey: "inputBrightness")
                filter?.setValue(3.272, forKey: "inputContrast")
                
            }
            else if currentFilterName == "MV2"
            {
                
                filter = CIFilter(name: "CIPhotoEffectTransfer")
                filter?.setDefaults()
                filter?.setValue(ciImage, forKey: kCIInputImageKey)
                
                let output2 = filter?.outputImage
                let cgimg2 = context.createCGImage(output2!, from: output2!.extent)
                
                let processedImage2 = UIImage(cgImage: cgimg2!)
                let ciImage2 = CIImage(image: processedImage2)
                
                filter = CIFilter(name: "CIColorControls")
                filter?.setDefaults()
                filter?.setValue(ciImage2, forKey: kCIInputImageKey)
                filter?.setValue(0.95, forKey: "inputSaturation")
                filter?.setValue(0.05, forKey: "inputBrightness")
                filter?.setValue(1.43125, forKey: "inputContrast")
                
                let output3 = filter?.outputImage
                let cgimg3 = context.createCGImage(output3!, from: output3!.extent)
                
                let processedImage3 = UIImage(cgImage: cgimg3!)
                let ciImage3 = CIImage(image: processedImage3)
                
                filter = CIFilter(name: "CIGaussianBlur")
                filter?.setDefaults()
                filter?.setValue(ciImage3, forKey: kCIInputImageKey)
                filter?.setValue(0.06, forKey: "inputRadius")
                
            }
            else if currentFilterName == "MV5"
            {
                
                filter = CIFilter(name: "CIPhotoEffectTransfer")
                filter?.setDefaults()
                filter?.setValue(ciImage, forKey: kCIInputImageKey)
                
                let output2 = filter?.outputImage
                let cgimg2 = context.createCGImage(output2!, from: output2!.extent)
                
                let processedImage2 = UIImage(cgImage: cgimg2!)
                let ciImage2 = CIImage(image: processedImage2)
                
                filter = CIFilter(name: "CIColorControls")
                filter?.setDefaults()
                filter?.setValue(ciImage2, forKey: kCIInputImageKey)
                filter?.setValue(1.03, forKey: "inputSaturation")
                filter?.setValue(0.15, forKey: "inputBrightness")
                filter?.setValue(1.675, forKey: "inputContrast")
                
                let output3 = filter?.outputImage
                let cgimg3 = context.createCGImage(output3!, from: output3!.extent)
                
                let processedImage3 = UIImage(cgImage: cgimg3!)
                let ciImage3 = CIImage(image: processedImage3)
                
                filter = CIFilter(name: "CIGaussianBlur")
                filter?.setDefaults()
                filter?.setValue(ciImage3, forKey: kCIInputImageKey)
                filter?.setValue(0.08, forKey: "inputRadius")
                
            }
            else if currentFilterName == "Vignette"
            {
                
                filter = CIFilter(name: "CIVignette")
                filter?.setDefaults()
                filter?.setValue(ciImage, forKey: kCIInputImageKey)
                filter?.setValue(1, forKey: "inputIntensity")
                filter?.setValue(2, forKey: "inputRadius")
                
            }
            else if currentFilterName == "Lomo"
            {
                
                filter = CIFilter(name: "CIPhotoEffectProcess")
                filter?.setDefaults()
                filter?.setValue(ciImage, forKey: kCIInputImageKey)
                
                let output2 = filter?.outputImage
                let cgimg2 = context.createCGImage(output2!, from: output2!.extent)
                
                let processedImage2 = UIImage(cgImage: cgimg2!)
                let ciImage2 = CIImage(image: processedImage2)
                
                filter = CIFilter(name: "CIColorControls")
                filter?.setDefaults()
                filter?.setValue(ciImage2, forKey: kCIInputImageKey)
                filter?.setValue(1.3694, forKey: "inputSaturation")
                filter?.setValue(0.3184, forKey: "inputBrightness")
                filter?.setValue(1.7308, forKey: "inputContrast")
                
                
                
                let output3 = filter?.outputImage
                let cgimg3 = context.createCGImage(output3!, from: output3!.extent)
                
                let processedImage3 = UIImage(cgImage: cgimg3!)
                let ciImage3 = CIImage(image: processedImage3)
                
                filter = CIFilter(name: "CIGaussianBlur")
                filter?.setDefaults()
                filter?.setValue(ciImage3, forKey: kCIInputImageKey)
                filter?.setValue(0.4, forKey: "inputRadius")
                
            }
            else if currentFilterName == "Twillight"
            {
                
                filter = CIFilter(name: "CIVignetteEffect")
                filter?.setDefaults()
                filter?.setValue(ciImage, forKey: kCIInputImageKey)
                filter?.setValue(CIVector(string: "[\(imageSize.width/2) \(imageSize.height/2)]"), forKey: "inputCenter")
                filter?.setValue(0.5, forKey: "inputIntensity")
                filter?.setValue(imageSize.height/2.5, forKey: "inputRadius")
                
            }
            else if currentFilterName == "B/W Cross"
            {
                
                filter = CIFilter(name: "CIMaximumComponent")
                filter?.setDefaults()
                filter?.setValue(ciImage, forKey: kCIInputImageKey)
                
                
                
                let output2 = filter?.outputImage
                let cgimg2 = context.createCGImage(output2!, from: output2!.extent)
                
                let processedImage2 = UIImage(cgImage: cgimg2!)
                let ciImage2 = CIImage(image: processedImage2)
                
                filter = CIFilter(name: "CIPhotoEffectProcess")
                filter?.setDefaults()
                filter?.setValue(ciImage2, forKey: kCIInputImageKey)
                
                
            }
            else if currentFilterName == "B/W Vintage"
            {
                
                filter = CIFilter(name: "CIMaximumComponent")
                filter?.setDefaults()
                filter?.setValue(ciImage, forKey: kCIInputImageKey)
                
                
                let output2 = filter?.outputImage
                let cgimg2 = context.createCGImage(output2!, from: output2!.extent)
                
                let processedImage2 = UIImage(cgImage: cgimg2!)
                let ciImage2 = CIImage(image: processedImage2)
                
                filter = CIFilter(name: "CIColorPosterize")
                filter?.setDefaults()
                filter?.setValue(ciImage2, forKey: kCIInputImageKey)
                filter?.setValue(5.62, forKey: "inputLevels")
                
                
                let output3 = filter?.outputImage
                let cgimg3 = context.createCGImage(output3!, from: output3!.extent)
                
                let processedImage3 = UIImage(cgImage: cgimg3!)
                let ciImage3 = CIImage(image: processedImage3)
                
                filter = CIFilter(name: "CIColorControls")
                filter?.setDefaults()
                filter?.setValue(ciImage3, forKey: kCIInputImageKey)
                filter?.setValue(0.02, forKey: "inputBrightness")
                
                
                
            }
            else if currentFilterName == "Sepia"
            {
                
                filter = CIFilter(name: "CISepiaTone")
                filter?.setDefaults()
                filter?.setValue(ciImage, forKey: kCIInputImageKey)
                filter?.setValue(1, forKey: "inputIntensity")
                
            }
            else if currentFilterName == "Cross Proc 1"
            {
                
                filter = CIFilter(name: "CIColorPolynomial")
                filter?.setDefaults()
                filter?.setValue(ciImage, forKey: kCIInputImageKey)
                filter?.setValue(CIVector(string: "[0 0.659 0 0.8607]"), forKey: "inputRedCoefficients")
                filter?.setValue(CIVector(string: "[0 0.6931 0 0.960]"), forKey: "inputGreenCoefficients")
                filter?.setValue(CIVector(string: "[0 0.480 0 0]"), forKey: "inputBlueCoefficients")
                filter?.setValue(CIVector(string: "[0 1 0 0]"), forKey: "inputAlphaCoefficients")
                
                let output2 = filter?.outputImage
                let cgimg2 = context.createCGImage(output2!, from: output2!.extent)
                
                let processedImage2 = UIImage(cgImage: cgimg2!)
                let ciImage2 = CIImage(image: processedImage2)
                
                filter = CIFilter(name: "CIPhotoEffectProcess")
                filter?.setDefaults()
                filter?.setValue(ciImage2, forKey: kCIInputImageKey)
                
            }
            else if currentFilterName == "Cross Proc 2"
            {
                
                filter = CIFilter(name: "CIPhotoEffectProcess")
                filter?.setDefaults()
                filter?.setValue(ciImage, forKey: kCIInputImageKey)
                
                let output3 = filter?.outputImage
                let cgimg3 = context.createCGImage(output3!, from: output3!.extent)
                
                let processedImage3 = UIImage(cgImage: cgimg3!)
                let ciImage3 = CIImage(image: processedImage3)
                
                
                
                filter = CIFilter(name: "CIColorControls")
                filter?.setDefaults()
                filter?.setValue(ciImage3, forKey: kCIInputImageKey)
                filter?.setValue(1.2484076, forKey: "inputSaturation")
                filter?.setValue(0.5, forKey: "inputBrightness")
                filter?.setValue(2.0772293, forKey: "inputContrast")
                
            }
            

        
        if let output = filter?.outputImage{
            if let cgimg = context.createCGImage(output, from: output.extent)
            {
                let processedImage = UIImage(cgImage: cgimg)
                return processedImage
            }
        }

        return processingImage
    }
    
    
    func blurGaussian() -> UIImage
       {
        let image = self.image
        var temp = (self.image!.size.width + self.image!.size.height)/2
        var redius = (temp * 2)/100 // 3 % redius of image size
           // let context = CIContext(options: nil)
           let ciImage = CIImage(image: image!)
        
          
           
           let lightBlur = CIFilter(name: "CIGaussianBlur")
           lightBlur?.setDefaults()
           lightBlur?.setValue(ciImage, forKey: kCIInputImageKey)
           lightBlur?.setValue(redius, forKey: kCIInputRadiusKey)
           
        let originalImage2CG = CIImage(cgImage: (self.image?.cgImage)!)
           
           if let output = lightBlur?.outputImage{
               if let cgimg = context.createCGImage(output, from: originalImage2CG.extent)
               {
                   let processedImage = UIImage(cgImage: cgimg)
                   return processedImage
                   
               }
           }
        return self.image!
       }
    
    func imageSizeAspectFit(imgview: UIImageView) -> CGSize {
        var newwidth: CGFloat
        var newheight: CGFloat
        let image: UIImage = self.image!
        
        if image.size.height >= image.size.width {
            newheight = imgview.frame.size.height;
            newwidth = (image.size.width / image.size.height) * newheight
            if newwidth > imgview.frame.size.width {
                let diff: CGFloat = imgview.frame.size.width - newwidth
                newheight = newheight + diff / newheight * newheight
                newwidth = imgview.frame.size.width
            }
        }
        else {
            newwidth = imgview.frame.size.width
            newheight = (image.size.height / image.size.width) * newwidth
            if newheight > imgview.frame.size.height {
                let diff: CGFloat = imgview.frame.size.height - newheight
                newwidth = newwidth + diff / newwidth * newwidth
                newheight = imgview.frame.size.height
            }
        }
        
       
        return CGSize(width:newwidth, height:newheight)
    }
    
    func imageOrientation(_ srcImage: UIImage)->UIImage {
           if srcImage.imageOrientation == UIImage.Orientation.up {
               return srcImage
           }
           var transform: CGAffineTransform = CGAffineTransform.identity
           switch srcImage.imageOrientation {
           case UIImageOrientation.down, UIImageOrientation.downMirrored:
               transform = transform.translatedBy(x: srcImage.size.width, y: srcImage.size.height)
               transform = transform.rotated(by: CGFloat(M_PI))// replace M_PI by Double.pi when using swift 4.0
               break
           case UIImageOrientation.left, UIImageOrientation.leftMirrored:
               transform = transform.translatedBy(x: srcImage.size.width, y: 0)
               transform = transform.rotated(by: CGFloat(M_PI_2))// replace M_PI_2 by Double.pi/2 when using swift 4.0
               break
           case UIImageOrientation.right, UIImageOrientation.rightMirrored:
               transform = transform.translatedBy(x: 0, y: srcImage.size.height)
               transform = transform.rotated(by: CGFloat(-M_PI_2))// replace M_PI_2 by Double.pi/2 when using swift 4.0
               break
           case UIImageOrientation.up, UIImageOrientation.upMirrored:
               break
           }
           switch srcImage.imageOrientation {
           case UIImageOrientation.upMirrored, UIImageOrientation.downMirrored:
               transform.translatedBy(x: srcImage.size.width, y: 0)
               transform.scaledBy(x: -1, y: 1)
               break
           case UIImageOrientation.leftMirrored, UIImageOrientation.rightMirrored:
               transform.translatedBy(x: srcImage.size.height, y: 0)
               transform.scaledBy(x: -1, y: 1)
           case UIImageOrientation.up, UIImageOrientation.down, UIImageOrientation.left, UIImageOrientation.right:
               break
           }
           let ctx:CGContext = CGContext(data: nil, width: Int(srcImage.size.width), height: Int(srcImage.size.height), bitsPerComponent: (srcImage.cgImage)!.bitsPerComponent, bytesPerRow: 0, space: (srcImage.cgImage)!.colorSpace!, bitmapInfo: CGImageAlphaInfo.premultipliedLast.rawValue)!
           ctx.concatenate(transform)
           switch srcImage.imageOrientation {
           case UIImageOrientation.left, UIImageOrientation.leftMirrored, UIImageOrientation.right, UIImageOrientation.rightMirrored:
               ctx.draw(srcImage.cgImage!, in: CGRect(x: 0, y: 0, width: srcImage.size.height, height: srcImage.size.width))
               break
           default:
               ctx.draw(srcImage.cgImage!, in: CGRect(x: 0, y: 0, width: srcImage.size.width, height: srcImage.size.height))
               break
           }
           let cgimg:CGImage = ctx.makeImage()!
           let img:UIImage = UIImage(cgImage: cgimg)
           return img
       }
    
      func imgViewSizeChange()
      {
      /*  if (self.ImageViewRatio != nil)
        {
            self.imgView.frame = self.ImageViewRatio
        }
       */
        self.aspectSize =  imageSizeAspectFit(imgview:self.imgView)
         
      
        imgView.frame.origin.x = (self.imageView2.frame.size.width - self.aspectSize.width)/2
        imgView.frame.origin.y = (self.imageView2.frame.size.height  - self.aspectSize.height)/2
        imgView.frame.size.width = self.aspectSize.width
        imgView.frame.size.height = self.aspectSize.height
    
      }
     
      func imgViewSizeDefault()
      {
        
        if ImageFrameRatioBG != nil && ImageViewRatio != nil
        {
            self.imgView.frame = ImageViewRatio
        }
        else
        {
            self.imgView.frame.size.width = self.imageFrameUiView.frame.size.width
            self.imgView.frame.size.height = self.imageFrameUiView.frame.size.height
            self.imgView.frame.origin.x = 0
            self.imgView.frame.origin.y = 0
        }
         
      }
    
      func filterCellImageSize()
      {
        if self.image != nil
        {
             if  Int((self.image?.size.width)!) > Int((self.image?.size.height)!)
                    {
                        self.filterCellImage = self.image?.resize(withWidth: 100)
                    }
                    else  if Int((self.image?.size.width)!) < Int((self.image?.size.height)!)
                    {
                        self.filterCellImage = self.image?.resize(withHeight: 100)
                    }
                    else
                    {
                        self.filterCellImage = self.image?.resize(withHeight: 100)
                    }
            
             self.filterCollectionView.reloadData()

        }
      }
    
       func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
           if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
                 self.image = imageOrientation(image)
                self.mainImage  = self.image!
                self.imgView.frame.size.width = self.imageFrameUiView.frame.size.width
                self.imgView.frame.size.height = self.imageFrameUiView.frame.size.height
                self.imgView.frame.origin.x = 0
                self.imgView.frame.origin.y = 0
                self.imgView.contentMode = .scaleAspectFit
                 self.imgView.image = self.image
               
               /* imageView2.frame.origin.x = 0
                imageView2.frame.origin.y = 0
                imageView2.frame.size.width = self.imageFrameUiView.frame.size.width
                imageView2.frame.size.height = self.imageFrameUiView.frame.size.height
                self.imageView2.image = self.mainImage
                 */
                //self.aspectSize = imageSizeAspectFit(imgview: self.imgView)
               //print("aspect new size: \(self.aspectSize)")
                       
                     //  imgView2?.image = self.mainImage
            
                
              
                
                 
                resetSize1()
                resetSize1()
            
                
             /*
             //mask code
             let maskImageView = UIImageView()
            maskImageView.image = UIImage(named: "shape2")
            maskImageView.frame = imgView.bounds
            imgView.mask = maskImageView
 */
             //   imgViewSizeChange()
              //  self.imgView.contentMode = .scaleAspectFill
            
                 self.sizeImageDetect = 1
                 self.resetImageDetect = 1
               
          
                self.filterCellImageSize()
           /* self.imgView.layer.borderColor =  UIColor.lightGray.cgColor
            self.imgView.layer.cornerRadius = 100.0
            self.imgView.layer.borderWidth = 15
            self.imgView.layer.masksToBounds = true
           */
           
                print("original image size: \(self.image)")
                self.originalImageWidth = self.image?.size.width
                self.originalImageHeight = self.image?.size.height
                self.collectionView.reloadData()
               
               
              // filterView()
               //vc!.sentImage = nil
               //vc!.sentImage = image
               
               
               //self.navigationController?.pushViewController(vc!, animated: true)
               
           }
           dismiss(animated: true, completion: nil)
       }

    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
           if segue.identifier == "goToEditView" {
               
               let exampleCropViewController = segue.destination as! ExampleCropViewController
            print("hello crop working")
           // print("print image size: \((sender as? UIImage)?.size)")
            exampleCropViewController.image = self.mainImage
            exampleCropViewController.delegate = self
               
           }
           
       }
       
       func stViewItemClick(img: UIImage) {
            imgView.image = img
             self.image = img
           print("stViewItemClick-------->")
       }
     
    
}


extension ViewController: IGRPhotoTweakViewControllerDelegate {
    func photoTweaksController(_ controller: IGRPhotoTweakViewController, didFinishWithCroppedImage croppedImage: UIImage) {
        self.imgView.image = croppedImage
        self.image = self.imgView.image
        print("croped complete")
        _ = controller.navigationController?.popViewController(animated: true)
    }
    
    func photoTweaksControllerDidCancel(_ controller: IGRPhotoTweakViewController) {
         print("croped finished")
         _ = controller.navigationController?.popViewController(animated: true)
        
    }
}



extension CALayer {

        func makeSnapshot() -> UIImage? {
        let scale = UIScreen.main.scale
        UIGraphicsBeginImageContextWithOptions(frame.size, false, scale)
        defer { UIGraphicsEndImageContext() }
        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        render(in: context)
        let screenshot = UIGraphicsGetImageFromCurrentImageContext()
        return screenshot
        }
}



extension ViewController: UICollectionViewDelegate, UICollectionViewDataSource{
    
        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
           
            if collectionView == self.collectionView
            {
                 return items.count
            }
            else
            {
                return filterItems.count
            }
            
            return items.count
            
        }
    
     
     func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView  ==  self.collectionView
        {
            let imageCell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! itemCollectionViewCell
            let itemName = items[indexPath.item].itemName
            
            if itemName == "new"
            {
               imageCell.sizeImage1.isHidden = true
               imageCell.sizeImage2.isHidden = true
               imageCell.sizeImage3.isHidden = true
                imageCell.itemImage.image = UIImage(named: "gallery")
                imageCell.itemLabel.text = "New"
                imageCell.isHidden = false
               // imageCell.itemLabel.isHidden = false
            }
            else if itemName == "crop"
            {
                imageCell.sizeImage1.isHidden = true
                imageCell.sizeImage2.isHidden = true
                imageCell.sizeImage3.isHidden = true
                 imageCell.itemImage.image = UIImage(named: "crop")
                 imageCell.itemLabel.text = "Crop"
                 imageCell.isHidden = false
            }
            else if itemName == "filter"
            {
                
                imageCell.sizeImage1.isHidden = true
                imageCell.sizeImage2.isHidden = true
                imageCell.sizeImage3.isHidden = true
                imageCell.itemImage.image = UIImage(named: "filter")
                imageCell.itemLabel.text = "Filter"
                
               
            }
            else if itemName == "color"
            {
                imageCell.sizeImage1.isHidden = true
                imageCell.sizeImage2.isHidden = true
                imageCell.sizeImage3.isHidden = true
                imageCell.itemImage.image = UIImage(named: "color")
                imageCell.itemLabel.text = "Color"
            }
            else if itemName == "canvas"
            {
               imageCell.sizeImage1.isHidden = true
               imageCell.sizeImage2.isHidden = true
               imageCell.sizeImage3.isHidden = true
               imageCell.itemImage.image = UIImage(named: "crop")
               imageCell.itemLabel.text = "Canvas"
            }
            else if itemName == "presets"
            {
                if self.resetImageDetect == 0
                  {
                     // imageCell.isHidden = true
                    
                  }
                  else if self.resetImageDetect == 1
                  {
                       imageCell.isHidden = false
                      imageCell.sizeImage1.image = UIImage(named: "circle_fill")
                      imageCell.sizeImage2.image = UIImage(named: "circle_gaap")
                      imageCell.sizeImage3.image = UIImage(named: "circle_gaap")
                  }
                  else if self.resetImageDetect == 2
                  {
                      imageCell.isHidden = false
                     
                      imageCell.sizeImage1.image = UIImage(named: "circle_gaap")
                      imageCell.sizeImage2.image = UIImage(named: "circle_fill")
                      imageCell.sizeImage3.image = UIImage(named: "circle_gaap")
                  }
                  else if self.resetImageDetect == 3
                  {
                      imageCell.isHidden = false
                      imageCell.sizeImage1.image = UIImage(named: "circle_gaap")
                      imageCell.sizeImage2.image = UIImage(named: "circle_gaap")
                      imageCell.sizeImage3.image = UIImage(named: "circle_fill")
                      self.resetImageDetect = 0
                     
                  }
                  
                  imageCell.sizeImage1.isHidden = false
                  imageCell.sizeImage2.isHidden = false
                  imageCell.sizeImage3.isHidden = false
                  imageCell.itemImage.image = UIImage(named: "presets")

                 
                  imageCell.itemLabel.text = "Presets"
            }
            else if itemName == "size"
            {
            
                if self.sizeImageDetect == 0
                {
                   // imageCell.isHidden = true
                  
                }
                else if self.sizeImageDetect == 1
                {
                     imageCell.isHidden = false
                    imageCell.sizeImage1.image = UIImage(named: "circle_fill")
                    imageCell.sizeImage2.image = UIImage(named: "circle_gaap")
                    imageCell.sizeImage3.image = UIImage(named: "circle_gaap")
                }
                else if self.sizeImageDetect == 2
                {
                    imageCell.isHidden = false
                   
                    imageCell.sizeImage1.image = UIImage(named: "circle_gaap")
                    imageCell.sizeImage2.image = UIImage(named: "circle_fill")
                    imageCell.sizeImage3.image = UIImage(named: "circle_gaap")
                }
                else if self.sizeImageDetect == 3
                {
                    imageCell.isHidden = false
                    imageCell.sizeImage1.image = UIImage(named: "circle_gaap")
                    imageCell.sizeImage2.image = UIImage(named: "circle_gaap")
                    imageCell.sizeImage3.image = UIImage(named: "circle_fill")
                    self.sizeImageDetect = 0
                   
                }
                
                imageCell.sizeImage1.isHidden = false
                imageCell.sizeImage2.isHidden = false
                imageCell.sizeImage3.isHidden = false
                imageCell.itemImage.image = UIImage(named: "size")

               
                imageCell.itemLabel.text = "Size"
            // imageCell.itemLabel.isHidden = false

            }
            
            
            
            return imageCell
        }
        else
        {
            let imageCell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier2, for: indexPath) as! filterCollectionViewCell
            let itemName = filterItems[indexPath.item].itemName
            
            imageCell.filterImage.layer.cornerRadius = 5.0
            imageCell.filterImage.clipsToBounds = true
            imageCell.celliconImage.layer.cornerRadius = 5.0
            imageCell.celliconImage.clipsToBounds = true
            imageCell.celliconImage.isHidden = true
            if(selectedFilter != nil && selectedFilter == indexPath.row){
                
                imageCell.celliconImage.isHidden = false
                imageCell.celliconImage.image = UIImage(named: "filter-overlay")
            }
            
           
            
            
            imageCell.filterImage.image =  applyEffectFilter(processingImage:  self.filterCellImage, currentFilterName: itemName)
            imageCell.filterLabel.text = itemName
            return imageCell

        }
          
         
     
        
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
         self.imageFrameUiView.backgroundColor = UIColor.white
       //   imgViewSizeDefault()
        if collectionView == self.collectionView
        {
            let itemName = items[indexPath.item].itemName
            self.slideShowCount = 0
            self.clickCount = 0
               
              if itemName == "new"
              {
               
                self.pickImage()
                imageView2.isHidden = true
                hasColorBG = nil
              }
              else if itemName == "crop"
              {
                 imageView2.isHidden = true
                 self.performSegue(withIdentifier: "goToEditView", sender: imgView.image)
              }
              else if itemName == "canvas"
              {
                 let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "canVasViewController") as? canVasViewController
                vc?.image = self.image
                vc?.orginalImage = self.mainImage
                        self.navigationController?.pushViewController(vc!, animated: true)
 
              }
              else if itemName == "color"
              {
                 let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "colorView") as? colorViewController
                 vc?.image = self.image
                 vc?.originalImage = self.mainImage
                 vc?.positionRationViewBg = self.imageFrameUiView.frame
                 self.clickSize1()
                 imgViewSizeChange()
               //self.imgView.contentMode = .scaleAspectFill
                 vc?.positionRatioView = self.imgView.frame
                self.navigationController?.pushViewController(vc!, animated: true)
              }
              else if itemName == "size"
              {
               
                 
                self.sizeImageDetect = self.sizeImageDetect + 1
               
                if self.sizeImageDetect == 1{
                    self.clickSize1()
                    imgViewSizeChange()
                    self.imgView.contentMode = .scaleAspectFill
                }
                else if self.sizeImageDetect == 2
                {
                    
                    self.clickSize2()
                  //  imgViewSizeChange()
                   // self.imgView.contentMode = .scaleAspectFill
                }
                else if self.sizeImageDetect == 3
                {
                  
                    self.clickSize3()
                    self.imgView.contentMode = .scaleAspectFill
                   
                }
                self.resetImageDetect = 1
                 self.collectionView.reloadData()
                
                
               
              }
              else if itemName == "presets"
              {
                  self.resetImageDetect = self.resetImageDetect + 1
                   
                   if self.resetImageDetect == 1{
                  
                   imgViewSizeDefault()
                    self.clickSize1()
                    imgViewSizeChange()
                     self.imgView.contentMode = .scaleAspectFill
                    imageView2.isHidden = true
                   }
                   else if self.resetImageDetect == 2
                   {
                    
                      self.clickSize1()
                      imgViewSizeChange()
                      self.imgView.contentMode = .scaleAspectFill
                     imageView2.image = blurGaussian()
                     imageView2.isHidden = false
                     
                   
                   }
                   else if self.resetImageDetect == 3
                   {
                    
                     self.clickSize1()
                       imgViewSizeChange()
                       self.imgView.contentMode = .scaleAspectFill
                       imageView2.image = self.image!
                       imageView2.isHidden = false
                      
                   }
                self.sizeImageDetect = 1
                    self.collectionView.reloadData()
              }
              else if itemName == "filter"
              {
                
                self.collectionView.isHidden = true
              //  takeScreenshot(view: self.imageFrameUiView)
              //  print("crop image size: \(self.cropeImage?.size)")
                //print("imageview size: \(self.imgView.frame.size)")
               // clickSize1()
                  //print("after imageview size: \(self.imgView.frame.size)")
                //self.imgView.image = self.cropeImage
                self.filterCollectionView.isHidden  = false
            }
           
        }
        else if collectionView ==  self.filterCollectionView
        {
          
            if(slideView.isHidden ==  false)
            {
                print("slide show")
                slideView.isHidden =  true
                slideShowCount = 1
                 filterCollectionView.reloadData()
            }
            else
            {
                if self.clickCount == 0
                {
                    
                    self.prevItem = indexPath[1]
                    self.nextItem = indexPath[1]
                    self.clickCount = 1
                }
                else
                {
                    self.prevItem = indexPath[1]
                    if self.prevItem != self.nextItem
                    {
                        self.slideShowCount  = 0
                        slideView.isHidden = true
                        self.nextItem = indexPath[1]
                    }
                }
                
                setupSlider1Defaults()
                setupSlider2Defaults()
                setupSlider3Defaults()
                setupSlider4Defaults()
                slideShowCount = slideShowCount + 1
                
                if  slideShowCount == 1
                {
                    let itemName = filterItems[indexPath.item].itemName
                    imgView.image = applyEffectFilter(processingImage: self.image!, currentFilterName: itemName)
                    self.filteredImage = imgView.image
                    self.image = self.filteredImage
                }
                else if slideShowCount == 2
                {
                   slideView.isHidden = false
                    
                }
                else
                {
                    slideView.isHidden = true
                    slideShowCount = 1
                }
                
                self.selectedFilter = indexPath.row
                self.filterCollectionView.reloadData()
            }
           
        }
        
            
    }
    
    
}



extension UIView {
        func makeSnapshot() -> UIImage? {
        if #available(iOS 10.0, *) {
        let renderer = UIGraphicsImageRenderer(size: frame.size)
        return renderer.image { _ in drawHierarchy(in: bounds, afterScreenUpdates: true) }
        } else {
        return layer.makeSnapshot()
        }
        }
}



//inorder to resize image with same aspect ratio
extension UIImage {
    
    //not used ... kept for future use
    func resize(withWidth newWidth: CGFloat) -> UIImage? {
        
        let scale = newWidth / self.size.width
        let newHeight = self.size.height * scale
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        self.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }
    
    func resize(withHeight newHeight: CGFloat) -> UIImage? {
        
        let scale = newHeight / self.size.height
        let newWidth = self.size.width * scale
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        self.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }
    
    func imageWithAlpha(alpha: CGFloat) -> UIImage {
           UIGraphicsBeginImageContextWithOptions(size, false, scale)
           draw(at: .zero, blendMode: .normal, alpha: alpha)
           let newImage = UIGraphicsGetImageFromCurrentImageContext()
           UIGraphicsEndImageContext()
           return newImage!
    }
    
}
