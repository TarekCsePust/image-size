//
//  colorViewController.swift
//  imageSize
//
//  Created by Odyssey_New on 11/7/19.
//  Copyright © 2019 Odyssey_New. All rights reserved.
//

import UIKit
import AVKit
import Photos

class colorViewController: UIViewController,UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout ,UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    var topSafeAreaHeight: CGFloat = 0
    var bottomSafeAreaHeight: CGFloat = 0
    var isEdit = true
    var isEditImg = true
    var image: UIImage!
    var originalImage:UIImage!
    var collectionView_color: UICollectionView!
    var collectionView_Gradient: UICollectionView!
    var collectionView_Pattern: UICollectionView!
    var ratioView: UIImageView!
    var ratioViewBg: UIImageView!
    var positionRatioView:CGRect!
    var positionRationViewBg:CGRect!
    var statusBarView = UIView()
    var colorImg:UIImage!
    var colorIndex:Int!
    
  //  @IBOutlet weak var bgMenuView: UIView!
    var bgMenuView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        self.setUpView()
        ratioView = UIImageView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 350))
        ratioViewBg = UIImageView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 350))
        ratioView.frame = positionRatioView
        ratioViewBg.frame = positionRationViewBg
        ratioViewBg.backgroundColor = UIColor.white
        ratioView.contentMode = .scaleAspectFit
        ratioView.image = self.image
        self.view.addSubview(ratioViewBg)
        self.ratioViewBg.addSubview(ratioView)
        bgMenuView = UIView(frame: CGRect(x: 0, y: 365, width: self.view.frame.width, height: 200))
        self.bgMenuView.backgroundColor = UIColor.black
        self.view.addSubview(bgMenuView)
        initBgColorCollectionView()
        initBgGradientCollectionView()
        initBgPatternCollectionView()
        
   
       
    }
    
    
    func setUpView() {
              if #available(iOS 11.0, *) {
                  statusBarView.frame = CGRect(x: 0, y: (UIApplication.shared.keyWindow?.safeAreaInsets.top)!, width: self.view.frame.width, height: 50)
                  if UIDevice.current.userInterfaceIdiom == .phone {
                      if UIScreen.main.nativeBounds.height == 2436 {
                          statusBarView.frame = CGRect(x: 0, y: (UIApplication.shared.keyWindow?.safeAreaInsets.top)! - 30, width: self.view.frame.width, height: 40)
                      }
                  }
              } else {
                  statusBarView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 30)
              }
              statusBarView.backgroundColor = UIColor(red: 18/255, green: 18/255, blue: 18/255, alpha: 1)
              self.view.addSubview(statusBarView)
              
              ///cancel button
              let cancelButton = UIButton()
              cancelButton.frame = CGRect(x: 5, y: statusBarView.frame.height/2 - 10, width: (self.view.frame.width - 10)/3, height: 30)
              cancelButton.setTitle(NSLocalizedString("Cancel", comment: "title"), for: .normal)
              cancelButton.setTitleColor(UIColor.white, for: .normal)
              cancelButton.titleLabel?.font = UIFont(name: "Helvetica", size: 15)
              cancelButton.contentHorizontalAlignment = .left
               cancelButton.addTarget(self, action: #selector(cancelButtonAction), for: .touchDown)
              statusBarView.addSubview(cancelButton)
              
              ///crop label
              let cropLabel = UILabel()
              cropLabel.frame = CGRect(x: cancelButton.frame.maxX, y: statusBarView.frame.height/2 - 10, width: (self.view.frame.width - 10)/3, height: 30)
              cropLabel.text = (NSLocalizedString("Color", comment: "title"))
              cropLabel.textColor = UIColor.white
              cropLabel.font = UIFont(name: "Helvetica-Bold", size: 18)
              cropLabel.textAlignment = .center
              statusBarView.addSubview(cropLabel)
              
              ///done button
              let doneButton = UIButton()
              doneButton.frame = CGRect(x: cropLabel.frame.maxX, y: statusBarView.frame.height/2 - 10, width: (self.view.frame.width - 10)/3, height: 30)
              doneButton.setTitle(NSLocalizedString("Done", comment: "title"), for: .normal)
              doneButton.setTitleColor(UIColor.white, for: .normal)
              doneButton.titleLabel?.font = UIFont(name: "Helvetica", size: 15)
              doneButton.contentHorizontalAlignment = .right
              doneButton.addTarget(self, action: #selector(doneButtonAction), for: .touchDown)
              statusBarView.addSubview(doneButton)
    }
    
    
    @objc func cancelButtonAction()
    {
        navigationController?.popViewController(animated: true)

        dismiss(animated: true, completion: nil)
    }
    
    @objc func doneButtonAction()
    {
         let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "viewController") as? ViewController
          vc?.ImageFrameRatioBG = ratioViewBg.frame
          vc?.ImageViewRatio = ratioView.frame
          if colorIndex != nil
          {
            vc?.colorIndex = self.colorIndex
            vc?.colorImage = nil
            vc?.hasColorBG = 1
          }
          else if colorImg != nil
          {
            vc?.colorImage = self.colorImg
             vc?.colorIndex = nil
             vc?.hasColorBG = 1
          }
          else
          {
            vc?.colorIndex = nil
            vc?.colorImage = nil
            vc?.hasColorBG = nil
          }
         
          vc?.image = self.image
          vc?.mainImage = self.originalImage
          self.navigationController?.pushViewController(vc!, animated: true)
       
    }
    
    func initBgColorCollectionView(){
        
        
        let label = UILabel()
        label.frame = CGRect(x: 10, y: 70-30, width: self.view.frame.width, height: 15)
        label.text = "Color"
        label.textColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1.0)
       
        label.font = UIFont.systemFont(ofSize: 9)
        label.numberOfLines = 0
        bgMenuView.addSubview(label)
        
        
        
        
        
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .horizontal
        
        let size = CGRect(x: 0, y: 85-30, width: self.view.frame.width, height: 40)
        collectionView_color = UICollectionView(frame: size, collectionViewLayout: flowLayout)
    
        collectionView_color.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "Cell_color")
        collectionView_color.delegate = self
        collectionView_color.dataSource = self
        collectionView_color.showsHorizontalScrollIndicator = false
        collectionView_color.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        bgMenuView.addSubview(collectionView_color)
        
        
        
        
    }
     func initBgGradientCollectionView(){
        
        
        let label = UILabel()
        label.frame = CGRect(x: 10, y: 125-30, width: self.view.frame.width, height: 15)
        label.text = "Gradient"
        label.textColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1.0)
        
        label.font = UIFont.systemFont(ofSize: 9)
        label.numberOfLines = 0
        bgMenuView.addSubview(label)
        
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .horizontal
        
        let size = CGRect(x: 0, y: 140-30, width: self.view.frame.width, height: 40)
        collectionView_Gradient = UICollectionView(frame: size, collectionViewLayout: flowLayout)
        
        collectionView_Gradient.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "Cell_gr")
        collectionView_Gradient.delegate = self
        collectionView_Gradient.dataSource = self
        collectionView_Gradient.showsHorizontalScrollIndicator = false
        collectionView_Gradient.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        bgMenuView.addSubview(collectionView_Gradient)
    }
    func initBgPatternCollectionView(){
        
        let label = UILabel()
        label.frame = CGRect(x: 10, y: 180-30, width: self.view.frame.width, height: 15)
        label.text = "Pattern"
        label.textColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1.0)
        
        label.font = UIFont.systemFont(ofSize: 9)
        label.numberOfLines = 0
        bgMenuView.addSubview(label)
        
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .horizontal
        
        let size = CGRect(x: 0, y: 195-30, width: self.view.frame.width, height: 40)
        collectionView_Pattern = UICollectionView(frame: size, collectionViewLayout: flowLayout)
        
        collectionView_Pattern.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "Cell_pa")
        collectionView_Pattern.delegate = self
        collectionView_Pattern.dataSource = self
        collectionView_Pattern.showsHorizontalScrollIndicator = false
        collectionView_Pattern.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        bgMenuView.addSubview(collectionView_Pattern)
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        switch collectionView {
            
        case collectionView_color:
            return ColorList().count
        case collectionView_Gradient:
            return 100
        default:
            // collectionView_Pattern
             return 100
        }
       
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        
        switch collectionView {
            
        case collectionView_color:
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell_color", for: indexPath)
            
            for view in cell.subviews {
                view.removeFromSuperview()
            }
            
            ///imageView
            let colorView = UIView()
            colorView.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
            let oColor = UIColor.init(red: 20/255 , green: 20/255, blue: 20/255, alpha: 1)
            
            colorView.backgroundColor = indexPath.row == 0 ? oColor : ColorList()[indexPath.row]
            colorView.layer.cornerRadius = 8
            cell.addSubview(colorView)
            
            ///imageView lock
         /*   if indexPath.row > 9 && !"f5videomixerpro".isPurchased() {
                let imageViewLock = UIImageView()
                imageViewLock.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
                imageViewLock.image = UIImage(named: "lock.png")
                cell.addSubview(imageViewLock)
            }
            */
            return cell
            
            
        case collectionView_Gradient:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell_gr", for: indexPath)
            
            for view in cell.subviews {
                view.removeFromSuperview()
            }
            
            // start 1.png - 100.png
            let grName = "\(indexPath.row+1).png"
            let grView = UIImageView()
            grView.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
            grView.image = UIImage(named: grName)
            grView.layer.cornerRadius = 8
            grView.clipsToBounds = true
            cell.addSubview(grView)
            
            
            ///imageView lock
          /*  if indexPath.row > 9 && !"f5videomixerpro".isPurchased() {
                let imageViewLock = UIImageView()
                imageViewLock.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
                imageViewLock.image = UIImage(named: "lock.png")
                cell.addSubview(imageViewLock)
            }*/
            return cell
        default:
            // colletction view pattern
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell_pa", for: indexPath)
            
            for view in cell.subviews {
                view.removeFromSuperview()
            }
            
            ///imageView
            
             // start 101.png - 200.png
            let imgName = "\(indexPath.row+101).png"
            let pattView = UIImageView()
            pattView.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
            pattView.image = UIImage(named: imgName)
            pattView.contentMode = .scaleToFill
            pattView.backgroundColor = .green
            pattView.layer.cornerRadius = 8
            pattView.clipsToBounds = true
            cell.addSubview(pattView)
            
            
            
            ///imageView lock
          /*  if indexPath.row > 9 && !"f5videomixerpro".isPurchased() {
                let imageViewLock = UIImageView()
                imageViewLock.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
                imageViewLock.image = UIImage(named: "lock.png")
                cell.addSubview(imageViewLock)
            }
            */
            return cell
            
           
       
            
          
            
           
        }
        
       
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        switch collectionView {
            
        case collectionView_color:
            return CGSize(width: 35, height: 35)
        case collectionView_Gradient:
            return CGSize(width: 35, height: 35)
         default:
                   // collectionView_Pattern
            return CGSize(width: 35, height: 35)
        
        }
        
        
        
       
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        
        switch collectionView {
            
        case collectionView_color:
             return 5.0
        case collectionView_Gradient:
            return 5.0
        default:
        // collectionView_Pattern
        return 5.0
       
        }
       
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        switch collectionView {
            
        case collectionView_color:
            return 5.0
        case collectionView_Gradient:
            return 5.0
        default:
            return 5.0
        
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        colorImg = nil
        colorIndex = nil
        switch collectionView {
            
        case collectionView_color:
            
            
            
            if(isEdit){
            
            
            ratioViewBg.image = image
            ratioViewBg.image = ratioViewBg.image?.withRenderingMode(.alwaysTemplate)
            ratioViewBg.tintColor = ColorList()[indexPath.row]
            colorIndex = indexPath.row
            isEditImg = false
            }
            
          /*  if(indexPath.row > 9 && !"f5videomixerpro".isPurchased()){
                self.performSegue(withIdentifier: "inAppPurchase", sender: self)
            }else{
                
                if(isEdit){
                
                blurView.alpha = 0.0
                ratioViewBg.image = image
                ratioViewBg.image = ratioViewBg.image?.withRenderingMode(.alwaysTemplate)
                ratioViewBg.tintColor = ColorList()[indexPath.row]
                isEditImg = false
                }
            }
            */
            
            
            
        case collectionView_Gradient:
           
            if(isEdit){
                           
               let grName = "\(indexPath.row+1).png"
               ratioViewBg.image = UIImage(named: grName)
                colorImg = UIImage(named: grName)
               isEditImg = false
               }
            /*if(indexPath.row > 9 && !"f5videomixerpro".isPurchased()){
                self.performSegue(withIdentifier: "inAppPurchase", sender: self)
            }else{
                
                  if(isEdit){
                
                let grName = "\(indexPath.row+1).png"
                ratioViewBg.image = UIImage(named: grName)
                blurView.alpha = 0.0
                isEditImg = false
                }
            }
            
           */
            
         default:
           //collection view pattern
            if(isEdit){
              
               let imgName = "\(indexPath.row+101).png"
               ratioViewBg.image = UIImage(named: imgName)
               colorImg =  UIImage(named: imgName)
               isEditImg = false
               }
           /* if(indexPath.row > 9 && !"f5videomixerpro".isPurchased()){
                self.performSegue(withIdentifier: "inAppPurchase", sender: self)
            }else{
                
                  if(isEdit){
                blurView.alpha = 0.0
                let imgName = "\(indexPath.row+101).png"
                ratioViewBg.image = UIImage(named: imgName)
                isEditImg = false
                }
            }
            */
          
        
            
        }
        
    }
        
        
      
        
    }
    

    


